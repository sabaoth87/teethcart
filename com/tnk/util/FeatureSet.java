package com.tnk.util;

import georegression.struct.point.Point2D_F64;

public class FeatureSet {
	private int index;
	private Point2D_F64 location;
	private double radius;
	
public FeatureSet(double inRadius, Point2D_F64 inLocale) {
	this.setRadius(inRadius);
	this.setLocation(inLocale);
	}

/**
 * @return the location
 */
public Point2D_F64 getLocation() {
	return location;
}

/**
 * @param location the location to set
 */
public void setLocation(Point2D_F64 location) {
	this.location = location;
}

/**
 * @return the radius
 */
public double getRadius() {
	return radius;
}

/**
 * @param radius the radius to set
 */
public void setRadius(double radius) {
	this.radius = radius;
}

/**
 * @return the index
 */
public int getIndex() {
	return index;
}

/**
 * @param index the index to set
 */
public void setIndex(int index) {
	this.index = index;
}

}
