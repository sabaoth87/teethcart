package com.tnk.util;

import georegression.struct.point.Point2D_F64;

public class FeaturesHandler {

	private FeatureSet[] featuresArray;
	private static int counter;
	
	public FeaturesHandler( FeatureSet[] fSetArray, int setSize) {
		
		//I am not sure if I will need the array from the main, or build a local one here..
		//featuresArray = fSetArray;
		featuresArray = new FeatureSet[setSize];
		setCounter(0);
	}
	
	public void addFeature( double radius, Point2D_F64 locale) {
		System.out.println("Adding " + radius + " to features to entry " + featuresArray.length);
		FeatureSet newFeature = new FeatureSet(radius, locale);
		
		//FeatureSet[] featuresArray2 = new FeatureSet[featuresArray.length + 1];
		//System.arraycopy(featuresArray, 0, featuresArray2, 0, featuresArray.length);
		//featuresArray2[featuresArray.length] = newFeature;
		featuresArray[counter] = newFeature;
		setCounter(getCounter() + 1);
		
	}
	
	public FeatureSet[] getFeatures() {
		return featuresArray;
		
	}

	/**
	 * @return the counter
	 */
	public static int getCounter() {
		return counter;
	}

	/**
	 * @param counter the counter to set
	 */
	public static void setCounter(int counter) {
		FeaturesHandler.counter = counter;
	}
	
}
