package com.tnk.util.items;

import java.awt.image.BufferedImage;

import com.tnk.util.Utilities.Tailor_Type;

import boofcv.io.image.UtilImageIO;

public class Tailor__Entry {

	//public ImageTailor tailor;
	public Tailor_Type type;
	public BufferedImage sourceImage;
	public float[] attribute;
	
	public Tailor__Entry (Tailor_Type tailorType,
						//ImageTailor tailorClass,
						String inputPath,
						float[] inputAttribute)
	{
		
		this.attribute = inputAttribute;
		//this.setTailor(tailorClass);
		this.setType(tailorType);
		this.sourceImage=UtilImageIO.loadImage(inputPath);
		
	}
	
	public Tailor__Entry (Tailor_Type tailorType,
						ImageTailor tailorClass,
						BufferedImage bImage,
						float[] inputAttribute)
	{
		
		this.attribute = inputAttribute;
		this.sourceImage = bImage;
		//this.setTailor(tailorClass);
		this.setType(tailorType);
		
	}

	/**
	 * @return the tailor
	 *//*
	public ImageTailor getTailor() {
		return tailor;
	}

	*//**
	 * @param tailor the tailor to set
	 *//*
	public void setTailor(ImageTailor tailor) {
		this.tailor = tailor;
	}*/

	/**
	 * @return the type
	 */
	public Tailor_Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Tailor_Type type) {
		this.type = type;
	}
	
	
	
}
