package com.tnk.util.items;

import java.awt.LayoutManager;

// TODO Need to find a way to continually run this operation until a pixel is selected.

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.tnk.layout.Layout_FirstScreen;
import com.tnk.util.SaveBIOutput;
import com.tnk.util.Utilities;

import boofcv.alg.color.ColorHsv;
import boofcv.gui.binary.VisualizeBinaryData;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;
import net.miginfocom.swing.MigLayout;

public class Tailor_ColourSegment extends ImageTailor {

	private BufferedImage sourceImage;
	private List<BufferedImage> outputImage = new ArrayList<>();
	private float attribute;

	private boolean selectingColour = true;
	
	
	public Tailor_ColourSegment(float attribute1,
								BufferedImage inputBI) {
		super(inputBI);
		this.sourceImage = inputBI;
		this.attribute = attribute1;
				
	}
	
	public Tailor_ColourSegment(float attribute1,
								String inputPath)
	{
		super(inputPath);
		this.sourceImage = UtilImageIO.loadImage(inputPath);
		this.attribute = attribute1;
	}
	
	@Override
	public void run() {
		
		printClickedColour(this.sourceImage);
		
	}	
	
	public void printClickedColour(final BufferedImage incoming) {
		BufferedImage image = incoming;
		// __ISSUE #008 [ENHANCE] Need to set restraints on the Colour Picker Size

		JFrame gui = new JFrame();
		LayoutManager guiMan;
		gui.setBounds(0, 0, 800,600); // for my small ass monitor :'(
		gui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		gui.getContentPane().setLayout(new MigLayout(      
				"",           // Layout Constraints
			      "[]20[]",   // Column constraints
			      "[]20[]"));    // Row constraints
		
		gui.add(new JLabel("Left"));
		gui.add(new JLabel("Right"), "wrap");
		
		
		ImagePanel imageFrame = new ImagePanel(image);
		imageFrame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				
				
				if (SwingUtilities.isLeftMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					/**
					 * MEMBERRY use this to grow an array of values to use for further segmentation
					 * effort. I want to be able to quickly 'teach' the program by clicking a batch
					 * of sample swatches that can be used by averaging out their value and
					 * segmenting similar areas. ie Click a couple shades of brown, it finds
					 * brown-ish ares in an image. I think this could be a stepping stone to
					 * teaching full blown features
					 */
					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					
					
					showSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);

					
				}

				if (SwingUtilities.isRightMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					removeSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);
					
				}

			}
		});
		
		ImageIcon previewIcon = new ImageIcon(image);
		
		JLabel displayPreview = new JLabel(previewIcon);
		displayPreview.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				
				
				if (SwingUtilities.isLeftMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					/**
					 * MEMBERRY use this to grow an array of values to use for further segmentation
					 * effort. I want to be able to quickly 'teach' the program by clicking a batch
					 * of sample swatches that can be used by averaging out their value and
					 * segmenting similar areas. ie Click a couple shades of brown, it finds
					 * brown-ish ares in an image. I think this could be a stepping stone to
					 * teaching full blown features
					 */
					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					
					
					showSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);

					
				}

				if (SwingUtilities.isRightMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					removeSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);
					
				}

			}
		});
		gui.add(displayPreview, "span");
		gui.pack();
		gui.setVisible(true);

		// _ENHANCE _
		// _CLEAN _
		// _FIX _
		// _ATTEMPT _
		// _METHOD _
		// _TEST _
		// _TRY _
		
		// _CLEAN The old way of showing the colour picker
		// cannot show the gui controls with the BoofCV ShowImages call
		//ShowImages.showWindow(imageFrame, "Click a colour area!", false);

	}
	
	/**
	 * Selectively displays only pixels which have a similar hue and saturation
	 * values to what is provided. This is intended to be a simple example of colour
	 * based segmentation. Colour based segmentation can be done in RGB colour, but
	 * more problematic due to it not being intensity invariant. More robust
	 * techniques can use Gaussian models instead of a uniform distribution, as is
	 * done below.
	 *
	 * @param name
	 * @param image
	 * @param hue
	 * @param saturation
	 */
	public void showSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;

				 //LABEL colour removing
				 if( dist2 <= maxDist2 ) {
				 output.setRGB(x,y,image.getRGB(x,y));
				 }

			}
		}

		ShowImages.showWindow(output, "Showing " + name);
		outputImages.add(output);
		selectingColour=false;
	}

	public void removeSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;


				// LABEL colour excluding?
				if (maxDist2 <= dist2) {
					output.setRGB(x, y, image.getRGB(x, y));
				}
			}
		}

		if (Utilities.debugMode) {
				Runnable outColSeg = new SaveBIOutput(output,"ClrSeg");
				new Thread(outColSeg).start();	
		}
		
		ShowImages.showWindow(output, "Showing " + name);
		outputImages.add(output);
		setOutput(outputImages);
		selectingColour=false;
	}
	
	/**
	 * LABEL     Proper way to send an output image to the main application
	 * MEMBERRY	 Call this once the thread containing this runnable has been run();
	 * 
	 * Retrieve the output of the operation once the Thread is run
	 * @return 
	 */
	public List<BufferedImage> getOutput() {
		return outputImages;
	}
	
	public void setOutput(List<BufferedImage> output) {
		Layout_FirstScreen.SetInProcessImage(output);
	}

}
