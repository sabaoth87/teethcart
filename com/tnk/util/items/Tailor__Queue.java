package com.tnk.util.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.Nullable;

import com.tnk.layout.Layout_FirstScreen;
import com.tnk.util.Utilities.DerivLevels;
import com.tnk.util.Utilities.Tailor_Type;

/*
 * _ENHANCE _WISHES Tailor_Queue
 * - need to add the code to the layout that adds each tailor to the stack
 *    -> then we can add the code to execute the stack once the button is pressed
 *    
 *    
 */

public class Tailor__Queue {

	private List<Tailor__Entry> tailorStack;

	public Tailor__Queue() {
		// create a new stack
		tailorStack = new ArrayList<>();
	}

	public void addToStack(Tailor__Entry entry) {
		tailorStack.add(entry);
	}

	public List<Tailor__Entry> getStack() {
		return tailorStack;
	}

	public void executeStack() {
		System.out.println("Tailor__Queue :: Executing Stack.");
		for (ListIterator<Tailor__Entry> iter = tailorStack.listIterator(); iter.hasNext();) {
			Tailor__Entry element = iter.next();
			System.out.println("Tailor__Queue :: Executing " + (iter.nextIndex()));
			if ((iter.nextIndex()-1)==0) {
				System.out.println("Tailor__Queue :: First! " + element.getType() + (iter.nextIndex()));
				executeFirstTailor(element.type, element.sourceImage, element.attribute, element);
			}
			executeTailor(element.type, Layout_FirstScreen.GetInProcessImages(), element.attribute);
		}

	}

	private void executeTailor(Tailor_Type type, List<BufferedImage> inProcessImages, float[] attribute) {

		System.out.println("Tailor__Queue :: executeTailor");

		switch (type) {
		case COLOURSEGMENT:
			System.out.println("Tailor__Queue :: ColourSeg Entry");
			/* Attributes
			 * [0] hue
			 * [1] saturation
			 */
			Tailor_ColourSegment colSeg = new Tailor_ColourSegment(attribute[0], inProcessImages.get(0));
			colSeg.run();

			break;

		case DERIVATIVE:
			System.out.println("Tailor__Queue :: Derivative Entry");
			Tailor_Derivative deriv = new Tailor_Derivative(DerivLevels.SOBELXnY, inProcessImages.get(0));
			deriv.run();

			break;

		case ELLIPSES:
			System.out.println("Tailor__Queue :: Ellipses Entry");
			Tailor_Ellipses ellipses = new Tailor_Ellipses(attribute[0], inProcessImages.get(0));
			ellipses.run();

			break;

		case LINEDETECTION:
			System.out.println("Tailor__Queue :: LineDetection Entry");
			Tailor_LineDetect lines = new Tailor_LineDetect(attribute[0], inProcessImages.get(0));
			lines.run();

			break;

		case SUPERPIXEL:
			System.out.println("Tailor__Queue :: Superpixel Entry");
			Tailor_Superpixel superpix = new Tailor_Superpixel(attribute[0], inProcessImages.get(0));
			superpix.run();

			break;

		case SURF:
			System.out.println("Tailor__Queue :: SURF Entry");
			Tailor_SURF surf = new Tailor_SURF(attribute[0], inProcessImages.get(0));
			surf.run();

			break;

		}

	}

	public void executeFirstTailor(Tailor_Type type,
			// ImageTailor tailor,
			BufferedImage sourceImage, float[] attribute, Tailor__Entry elem) {
		System.out.println("Tailor__Queue :: executeFirstTailor");
		switch (type) {
		case COLOURSEGMENT:
			System.out.println("Tailor__Queue :: FIRST! ColourSeg Entry");
			Tailor_ColourSegment colSeg = new Tailor_ColourSegment(attribute[0], sourceImage);
			colSeg.run();

			break;

		case DERIVATIVE:
			System.out.println("Tailor__Queue :: FIRST! Derivative Entry");
			Tailor_Derivative deriv = new Tailor_Derivative(DerivLevels.SOBELXnY, sourceImage);
			deriv.run();

			break;

		case ELLIPSES:
			System.out.println("Tailor__Queue :: FIRST! Ellipses Entry");
			Tailor_Ellipses ellipses = new Tailor_Ellipses(attribute[0], sourceImage);
			ellipses.run();

			break;

		case LINEDETECTION:
			System.out.println("Tailor__Queue :: FIRST! LineDetection Entry");
			Tailor_LineDetect lines = new Tailor_LineDetect(attribute[0], sourceImage);
			lines.run();

			break;

		case SUPERPIXEL:
			System.out.println("Tailor__Queue :: FIRST! Superpixel Entry");
			Tailor_Superpixel superpix = new Tailor_Superpixel(attribute[0], sourceImage);
			superpix.run();

			break;

		case SURF:
			System.out.println("Tailor__Queue :: FIRST! SURF Entry");
			Tailor_SURF surf = new Tailor_SURF(attribute[0], sourceImage);
			surf.run();

			break;

		}

	}

}
