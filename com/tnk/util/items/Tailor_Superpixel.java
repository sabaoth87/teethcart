package com.tnk.util.items;

import java.awt.image.BufferedImage;

import org.ddogleg.struct.FastQueue;
import org.ddogleg.struct.GrowQueue_I32;

import com.tnk.util.SaveBIOutput;
import com.tnk.util.Utilities;

import boofcv.abst.segmentation.ImageSuperpixels;
import boofcv.alg.filter.blur.GBlurImageOps;
import boofcv.alg.segmentation.ComputeRegionMeanColor;
import boofcv.alg.segmentation.ImageSegmentationOps;
import boofcv.factory.segmentation.ConfigFh04;
import boofcv.factory.segmentation.FactoryImageSegmentation;
import boofcv.factory.segmentation.FactorySegmentationAlg;
import boofcv.gui.feature.VisualizeRegions;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.feature.ColorQueue_F32;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayS32;
import boofcv.struct.image.ImageBase;
import boofcv.struct.image.ImageType;
import boofcv.struct.image.Planar;

public class Tailor_Superpixel extends ImageTailor {

	// private BufferedImage sourceImage;
	// private List<BufferedImage> outputImage = new ArrayList<>();
	// private float attribute;

	/**
	 * Operation
	 * 
	 * Depending on the operation, there can be multiple attributes
	 * 
	 * @param attribute
	 */

	// constructor
	public Tailor_Superpixel(float attribute, // this is where we can have the triggers to execute differ
			BufferedImage inputImage) {
		super(inputImage);
		this.sourceImage = inputImage;
		this.attribute = attribute;
	}

	public Tailor_Superpixel(float attribute, // this is where we can have the triggers to execute differ
			String inputPath) {
		super(inputPath);
		this.sourceImage = UtilImageIO.loadImage(inputPath);
		this.attribute = attribute;
	}

	@Override
	public void run() {
		BufferedImage image = this.sourceImage;

		image = ConvertBufferedImage.stripAlphaChannel(image);
		// Select input image type. Some algorithms behave different depending on image
		// type
		ImageType<Planar<GrayF32>> imageType = ImageType.pl(3, GrayF32.class);
		ImageSuperpixels alg = FactoryImageSegmentation.fh04(new ConfigFh04(100, 30), imageType);
		// Convert image into BoofCV format
		ImageBase colour = imageType.createImage(image.getWidth(), image.getHeight());
		ConvertBufferedImage.convertFrom(image, colour, true);

		// Segment and display results
		performSegmentation(alg, colour);
	}

	public <T extends ImageBase<? super T>> void performSegmentation(ImageSuperpixels<? super T> alg, T colour) {
		// Segmentation often works better after blurring the image. Reduces high
		// frequency image components which
		// can cause over segmentation
		GBlurImageOps.gaussian(colour, colour, 0.5, -1, null);

		// Storage for segmented image. Each pixel will be assigned a label from 0 to
		// N-1, where N is the number
		// of segments in the image
		GrayS32 pixelToSegment = new GrayS32(colour.width, colour.height);

		// Segmentation magic happens here
		alg.segment(colour, pixelToSegment);
		System.out.println(" fh04 found " + alg.getTotalSuperpixels() + " SuperPixels");

		if (alg.getTotalSuperpixels() < 200) {
		for (int inc = 0; inc < alg.getTotalSuperpixels() ; inc++) {

			System.out.println(" Contours? indexToPixel[" + inc 
					+ "] x:" + pixelToSegment.indexToPixel(inc).getX() 
					+ " y: " + pixelToSegment.indexToPixel(inc).getY() 
					+ " dimension " + pixelToSegment.indexToPixel(inc).getDimension()
					+ " data " +pixelToSegment.data[inc]);
		}
		}
		// _TEST

		int[] datas = new int[pixelToSegment.getData().length];
		pixelToSegment.getData();
		datas = pixelToSegment.data;

		System.out.println(" Index (1,1) " + pixelToSegment.getIndex(1, 1));
		// Fh04_to_ImageSuperpixels(pixelToSegment, ConnectRule.FOUR);
		// Displays the results
		visualize(pixelToSegment, colour, alg.getTotalSuperpixels());

	}

	/**
	 * Visualizes results three ways. 1) Colorized segmented image where each region
	 * is given a random color. 2) Each pixel is assigned the mean color through out
	 * the region. 3) Black pixels represent the border between regions.
	 */
	public <T extends ImageBase<? super T>> void visualize(GrayS32 pixelToRegion, T colour, int numSegments) {
		// Computes the mean color inside each region
		ImageType<? super T> type = colour.getImageType();
		ComputeRegionMeanColor<? super T> colorize = FactorySegmentationAlg.regionMeanColor(type);

		FastQueue<float[]> segmentColor = new ColorQueue_F32(type.getNumBands());
		segmentColor.resize(numSegments);

		GrowQueue_I32 regionMemberCount = new GrowQueue_I32();
		regionMemberCount.resize(numSegments);

		ImageSegmentationOps.countRegionPixels(pixelToRegion, numSegments, regionMemberCount.data);
		colorize.process(colour, pixelToRegion, regionMemberCount, segmentColor);

		GrayS32 testRegions = new GrayS32();
		testRegions = pixelToRegion.clone();

		ImageSegmentationOps.regionPixelId_to_Compact(pixelToRegion, regionMemberCount, testRegions);

		for (int inc1 = 0; inc1 < 10; inc1++) {
			 System.out.println(" segmentColor.data.length " + inc1
			 +"] data" + segmentColor.data.length);

			// increment both levels of the array
			//System.out.println(" segmentColor data ["+inc1+"]["+inc1+"] " + segmentColor.data[inc1][inc1]);
			System.out.println(" segmentColor data ["+inc1+"][0] " + segmentColor.data[inc1][0]);
			
		}
		// _TEST HIGH MEM WARNING
		// this may tax the ALU if the image is too complex
		// _ENHANCE need to figure out a way to handle, or understand the regions

		/*
		 * for (int i = 0; i < regionMemberCount.size; i++) {
		 * System.out.println("Membercount["+i+"] " + regionMemberCount.get(i)
		 * +" data["+i+"] " + regionMemberCount.data[i]); }
		 */

		// System.out.println(" testRegions " + testRegions.);;
		System.out.println(" segmentColor data [0][0] " + segmentColor.data[0][0]);

		// Draw each region using their average color
		BufferedImage outColour = VisualizeRegions.regionsColor(pixelToRegion, segmentColor, null);
		// Draw each region by assigning it a random color
		BufferedImage outSegments = VisualizeRegions.regions(pixelToRegion, numSegments, null);

		// Make region edges appear red
		BufferedImage outBorder = new BufferedImage(colour.width, colour.height, BufferedImage.TYPE_INT_RGB);
		ConvertBufferedImage.convertTo(colour, outBorder, true);
		VisualizeRegions.regionBorders(pixelToRegion, 0xFF0000, outBorder);

		if (Utilities.debugMode) {
			// Output the beautiful colours
			Runnable r = new SaveBIOutput(outColour, "sp_colour");
			new Thread(r).start();
			// Output the border outlines
			Runnable r1 = new SaveBIOutput(outBorder, "sp_border");
			new Thread(r1).start();
			// Output the segments
			Runnable r2 = new SaveBIOutput(outSegments, "sp_segments");
			new Thread(r2).start();
		}

		outputImages.add(outColour);
		outputImages.add(outBorder);
		outputImages.add(outSegments);

		this.setOutput(outputImages);

	}

}
