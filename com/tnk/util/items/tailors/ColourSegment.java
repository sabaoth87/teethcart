package com.tnk.util.items.tailors;

import java.awt.image.BufferedImage;

import com.tnk.util.items.ImageTailor;

import boofcv.alg.color.ColorHsv;
import boofcv.gui.image.ShowImages;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;

public class ColourSegment extends ImageTailor {

	public ColourSegment(float attribute1, BufferedImage inputBI) {
		super(inputBI);
		this.sourceImage = inputBI;
		this.attribute = attribute1;

	}

	public ColourSegment(float attribute1, String inputPath) {
		super(inputPath);
		this.sourceImage = UtilImageIO.loadImage(inputPath);
		this.attribute = attribute1;
	}

	@Override
	public void run() {
		
		showSelectedColour("Yellow",sourceImage,1f,1f);
		showSelectedColour("Green",sourceImage,1.5f,0.65f);
	}
	
	
	
	/**
	 * Selectively displays only pixels which have a similar hue and saturation
	 * values to what is provided. This is intended to be a simple example of colour
	 * based segmentation. Colour based segmentation can be done in RGB colour, but
	 * more problematic due to it not being intensity invariant. More robust
	 * techniques can use Gaussian models instead of a uniform distribution, as is
	 * done below.
	 *
	 * @param name
	 * @param image
	 * @param hue
	 * @param saturation
	 */
	public void showSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;

				// LABEL colour removing
				if (dist2 <= maxDist2) {
					output.setRGB(x, y, image.getRGB(x, y));
				}

			}
		}

		ShowImages.showWindow(output, "Showing " + name);
		outputImages.add(output);
	}

}
