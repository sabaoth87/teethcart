package com.tnk.util.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.tnk.layout.Layout_FirstScreen;
import com.tnk.util.SaveBIOutput;
import com.tnk.util.Utilities.DerivLevels;

import boofcv.abst.filter.derivative.AnyImageDerivative;
import boofcv.alg.filter.derivative.DerivativeType;
import boofcv.alg.filter.derivative.GImageDerivativeOps;
import boofcv.core.image.border.BorderType;
import boofcv.gui.image.VisualizeImageData;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;


public class Tailor_Derivative implements Runnable{

	private String opName = "Derivatives";
	private DerivLevels derivLevel;
	private BufferedImage sourceImage;
	//public BufferedImage[] outputImages;
	private static List<BufferedImage> outputImages = new ArrayList<>();
	// Derivative cases
	int deriv_SobelX = 0;
	
	/**
	 * Operation
	 * 
	 * Depending on the operation, there can be multiple attributes
	 * 
	 * @param attribute
	 */
	
	// constructors
	public Tailor_Derivative( DerivLevels derivativeLevel, // attribute 1
								 BufferedImage inputBI
								) 
	{
		derivLevel = derivativeLevel;
		sourceImage = inputBI;
	}
	
	public Tailor_Derivative( DerivLevels derivativeLevel,
								 String inputPath)
	{
		derivLevel = derivativeLevel;
		sourceImage = UtilImageIO.loadImage(inputPath);
	}
		
	@Override
	public void run() {
		
		GrayF32 gray = new GrayF32(sourceImage.getWidth(), sourceImage.getHeight());
		ConvertBufferedImage.convertFrom(sourceImage, gray);
		
		// First order derivative, also known as the gradient
					GrayF32 derivX = new GrayF32(gray.width, gray.height);
					GrayF32 derivY = new GrayF32(gray.width, gray.height);
					
		// Second order derivative, also known as the Hessian
					GrayF32 derivXX = new GrayF32(gray.width, gray.height);
					GrayF32 derivXY = new GrayF32(gray.width, gray.height);
					GrayF32 derivYY = new GrayF32(gray.width, gray.height);
		
		
		switch (derivLevel) {
		
		case SOBELGRAY :
			
			BufferedImage gray00 = new BufferedImage(gray.width, gray.height, deriv_SobelX);
			ConvertBufferedImage.convertTo(gray, gray00);
			
			Runnable outGray = new SaveBIOutput(gray00, "grey_output");
			new Thread(outGray).start();
			outputImages.add(gray00);
			
			break;
		
		case SOBELX :
			
			// LABEL DerivX Output
			GImageDerivativeOps.gradient(DerivativeType.SOBEL, gray, derivX, derivY, BorderType.EXTENDED);

			BufferedImage outputX = VisualizeImageData.colorizeSign(derivX, null, -1);
			Runnable outX = new SaveBIOutput(outputX, "DerivX_output_colourized");
			new Thread(outX).start();
			
			outputImages.add(outputX);
			
			break;
			
		case SOBELY : 
			
			// LABEL DerivY Output
			GImageDerivativeOps.gradient(DerivativeType.SOBEL, gray, derivX, derivY, BorderType.EXTENDED);

			BufferedImage outputY = VisualizeImageData.colorizeSign(derivY, null, -1);
			Runnable outY = new SaveBIOutput(outputY, "DerivY_output_colourized");
			new Thread(outY).start();
			
			outputImages.add(outputY);
			
			break;
			
		case SOBELXnY :
			
			GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
					BorderType.EXTENDED);
			
			BufferedImage outputXnY = VisualizeImageData.colorizeGradient(derivX, derivY, -1);
			Runnable outXnY = new SaveBIOutput(outputXnY, "DerivXnY_output_colourized");
			new Thread(outXnY).start();
			
			outputImages.add(outputXnY);
			
			break;
			
		case SOBELXX :
			
			
			GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
					BorderType.EXTENDED);
			
			BufferedImage outputXX = VisualizeImageData.colorizeSign(derivXX, null, -1);
			outputImages.add(outputXX);
			
			break;
			
		case SOBELXY :
			
			
			GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
					BorderType.EXTENDED);			
			
			BufferedImage outputXY = VisualizeImageData.colorizeSign(derivXY, null, -1);
			outputImages.add(outputXY);
			
			break;
			
		case SOBELYY :
			
			
			GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
					BorderType.EXTENDED);
			
			BufferedImage outputYY = VisualizeImageData.colorizeSign(derivYY, null, -1);
			outputImages.add(outputYY);
			
			break;
			
		case SOBELXYX:
			// There's also a built in function for computing arbitrary derivatives
			AnyImageDerivative<GrayF32, GrayF32> derivative = GImageDerivativeOps.createAnyDerivatives(DerivativeType.SOBEL,
					GrayF32.class, GrayF32.class);

			
			// the boolean sequence indicates if its an X or Y derivative
			derivative.setInput(gray);
			GrayF32 derivXYX = derivative.getDerivative(true, false, true);
			BufferedImage outputXYX = VisualizeImageData.colorizeSign(derivXYX, null, -1);
			outputImages.add(outputXYX);
			break;
		}
		setOutput(outputImages);
	}
	
	public void setOutput(List<BufferedImage> output) {
		Layout_FirstScreen.SetInProcessImage(output);
	}

	/**
	 * @return the opName
	 */
	public String getOpName() {
		return opName;
	}

	/**
	 * @param opName the opName to set
	 */
	public void setOpName(String opName) {
		this.opName = opName;
	}



	
	
	
}


