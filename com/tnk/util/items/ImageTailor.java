package com.tnk.util.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.tnk.layout.Layout_FirstScreen;

import boofcv.io.image.UtilImageIO;

public class ImageTailor implements Runnable {

	public BufferedImage sourceImage;
	public static List<BufferedImage> outputImages = new ArrayList<>();
	public float attribute;

	public ImageTailor ( BufferedImage inputBI)
	{
		setSourceImage(inputBI);
	}
	
	public ImageTailor (String inputPath)
	{
		setSourceImage(UtilImageIO.loadImage(inputPath));
	}
	
	@Override
	public void run() {
		
	}
	
	/**
	 * LABEL     Proper way to send an output image to the main application
	 * MEMBERRY	 Call this once the thread containing this runnable has been run();
	 * 
	 * Retrieve the output of the operation once the Thread is run
	 * @return 
	 */
	public List<BufferedImage> getOutput() {
		return outputImages;
	}

	/**
	 * @return the sourceImage
	 */
	public BufferedImage getSourceImage() {
		return sourceImage;
	}

	/**
	 * @param sourceImage the sourceImage to set
	 */
	public void setSourceImage(BufferedImage sourceImage) {
		this.sourceImage = sourceImage;
	}
	
	/**
	 * LABEL ImageTailor setOutput()
	 * 
	 * @param output
	 */
	public void setOutput(List<BufferedImage> output) {
		Layout_FirstScreen.SetInProcessImage(output);
	}
	
}
