package com.tnk.util.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Template_Singleton {

	// Static member holds only one instance of the
	// Tailor__Queue class
	private static Template_Singleton singletonInstance;
	public List<BufferedImage> images = new ArrayList<>();
	
	// Singleton prevents any other class from instantiating
	private Template_Singleton() {
		// TODO I am not sure if i will need a constructor for this one...
	}
	
	// Providing Global point of access
	public static Template_Singleton getSingletonInstance() {
		if (null == singletonInstance) {
			singletonInstance = new Template_Singleton();
		}
		return singletonInstance;
	}
}

