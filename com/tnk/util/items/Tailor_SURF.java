package com.tnk.util.items;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Hashtable;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.tnk.util.SaveBIOutput;

import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.abst.feature.detect.interest.InterestPointDetector;
import boofcv.factory.feature.detect.interest.FactoryInterestPoint;
import boofcv.gui.feature.FancyInterestPointRender;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.ImageGray;
import georegression.struct.point.Point2D_F64;
import net.miginfocom.swing.MigLayout;

public class Tailor_SURF extends ImageTailor {

	private String opName = "Tailor_SURF";
	private static BufferedImage sourceImage;
	public static BufferedImage sourceImageBuffer;
	public BufferedImage workerImage;
	private float attribute;

	private String imagePath;
	public JSlider slider_detectThreshold;
	public JSlider slider_extractRadius;
	public JSlider slider_maxFeaturesPerScale;
	public JSlider slider_initialSampleSize;
	public JSlider slider_initialSize;
	public JSlider slider_numberScalesPerOctave;
	public JSlider slider_numberOfOctaves;
	
	public JButton button_refreshImage;

	/*
	 * NOTEME Default values based on ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));
	 */

	public static float val_detectThreshold = 10f;
	public static int val_extractRadius = 2;
	public static int val_maxFeaturesPerScale = 10;
	public static int val_initialSampleSize = 2;
	public static int val_initialSize = 9;
	public static int val_numberScalesPerOctave = 3;
	public static int val_numberOfOctaves = 4;

	public static JFrame GUI;

	private static ImagePanel imagePanel; 
	
	public static Graphics2D g2;
	public static FancyInterestPointRender render;
	public static InterestPointDetector<ImageGray> detector;
	/**
	 * Operation
	 * 
	 * Depending on the operation, there can be multiple attributes
	 * 
	 * @param attribute
	 */

	// constructor
	public Tailor_SURF(float inAttribute, // this is where we can have the triggers to execute differ
			BufferedImage inSourceImage) {
		super(inSourceImage);
		this.attribute = inAttribute;
		sourceImage = inSourceImage;
		sourceImageBuffer = inSourceImage;

		initiailizeGUI();

	}

	public Tailor_SURF(float inAttribute, String inputPath) {
		super(inputPath);
		this.imagePath = inputPath;
		sourceImage = UtilImageIO.loadImage(inputPath);
		sourceImageBuffer = UtilImageIO.loadImage(inputPath);
		this.attribute = inAttribute;

		initiailizeGUI();

	}

	/**
	 * @return the opName
	 */
	public String getOpName() {
		return opName;
	}

	/**
	 * @param opName
	 *            the opName to set
	 */
	public void setOpName(String opName) {
		this.opName = opName;
	}

	private void initiailizeGUI() {

		GUI = new JFrame();
		// GUI.setBounds(0, 0, 1366, 768); // for my small ass monitor :'(
		GUI.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI.getContentPane().setLayout(new MigLayout(" ", "[fill][grow][fill]", "[fill][fill][fill]"));

		slider_detectThreshold = new JSlider();
		slider_detectThreshold.setMaximum(20);
		slider_detectThreshold.setMinimum(0);
		slider_detectThreshold.setValue(10);
		// Create the label table
		Hashtable labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(10), new JLabel("Threshold"));
		slider_detectThreshold.setLabelTable(labelTable);
		slider_detectThreshold.setPaintLabels(true);
		// val_detectThreshold =
		// Float.intBitsToFloat(slider_detectThreshold.getValue());
		slider_detectThreshold.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_detectThreshold = slider.getValue();
				
				BufferedImage workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);
				
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
			}
		});

		slider_extractRadius = new JSlider();
		slider_extractRadius.setMaximum(10);
		slider_extractRadius.setMinimum(0);
		slider_extractRadius.setValue(2);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(5), new JLabel("Radius"));
		slider_extractRadius.setLabelTable(labelTable);
		slider_extractRadius.setPaintLabels(true);
		// val_extractRadius = slider_extractRadius.getValue();
		slider_extractRadius.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_extractRadius = slider.getValue();
				
				BufferedImage workerBI;
				workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
			
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
				}
		});

		slider_maxFeaturesPerScale = new JSlider();
		slider_maxFeaturesPerScale.setMaximum(200);
		slider_maxFeaturesPerScale.setMinimum(0);
		slider_maxFeaturesPerScale.setValue(100);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(100), new JLabel("Max Per Scale"));
		slider_maxFeaturesPerScale.setLabelTable(labelTable);
		slider_maxFeaturesPerScale.setPaintLabels(true);
		// val_maxFeaturesPerScale = slider_maxFeaturesPerScale.getValue();
		slider_maxFeaturesPerScale.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_maxFeaturesPerScale = slider.getValue();
				
				BufferedImage workerBI;
				workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
			
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
				}
		});

		slider_initialSampleSize = new JSlider();
		slider_initialSampleSize.setMaximum(5);
		slider_initialSampleSize.setMinimum(0);
		slider_initialSampleSize.setValue(2);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(slider_initialSampleSize.getMaximum() / 2), new JLabel("Initial Sample"));
		slider_initialSampleSize.setLabelTable(labelTable);
		slider_initialSampleSize.setPaintLabels(true);
		// val_initialSampleSize = slider_initialSampleSize.getValue();
		slider_initialSampleSize.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_initialSampleSize = slider.getValue();
				
				BufferedImage workerBI;
				workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
				
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
				}
		});

		slider_initialSize = new JSlider();
		slider_initialSize.setMaximum(20);
		slider_initialSize.setMinimum(0);
		slider_initialSize.setValue(9);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(10), new JLabel("Initial Size"));
		slider_initialSize.setLabelTable(labelTable);
		slider_initialSize.setPaintLabels(true);
		// val_initialSize = slider_initialSize.getValue();
		slider_initialSize.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_initialSize = slider.getValue();
				
				BufferedImage workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
				
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
				}
		});
		

		slider_numberScalesPerOctave = new JSlider();
		slider_numberScalesPerOctave.setMaximum(10);
		slider_numberScalesPerOctave.setMinimum(0);
		slider_numberScalesPerOctave.setValue(4);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(5), new JLabel("Scales per Octave"));
		slider_numberScalesPerOctave.setLabelTable(labelTable);
		slider_numberScalesPerOctave.setPaintLabels(true);
		// val_numberScalesPerOctave = slider_numberScalesPerOctave.getValue();
		slider_numberScalesPerOctave.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_numberScalesPerOctave = slider.getValue();
				
				BufferedImage workerBI;
				workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
				
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
			}
		});
		

		slider_numberOfOctaves = new JSlider();
		slider_numberOfOctaves.setMaximum(10);
		slider_numberOfOctaves.setMinimum(0);
		slider_numberOfOctaves.setValue(4);
		// Create the label table
		labelTable = new Hashtable();
		// labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		// labelTable.put( new Integer( 10 ), new JLabel("Slow") );
		labelTable.put(new Integer(5), new JLabel("Octaves"));
		slider_numberOfOctaves.setLabelTable(labelTable);
		slider_numberOfOctaves.setPaintLabels(true);
		// val_numberOfOctaves = slider_numberOfOctaves.getValue();
		slider_numberOfOctaves.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider slider = (JSlider) e.getSource();
				val_numberOfOctaves = slider.getValue();
				
				BufferedImage workerBI;
				workerBI = workerImage;
				
				DetectFeatures(workerBI, GrayF32.class);			
				
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel = new ImagePanel(workerBI);
				//imagePanel.repaint();
				//GUI.repaint();
			}
		});
		
		ImageIcon icon_bolt = createImageIcon("D:\\Programming\\Workspaces\\BitBucket\\teethcart\\src\\icons\\icon_bolt.png");
        ImageIcon icont_book = createImageIcon("D:\\\\Programming\\\\Workspaces\\\\BitBucket\\\\teethcart\\\\src\\\\icons\\\\icon_book.png");
        ImageIcon icon_plus = createImageIcon("src\\icons\\icon_plus.png");
        ImageIcon icon_minus = createImageIcon("src\\icons\\icon_minus.png");
        ImageIcon icon_img = createImageIcon("D:\\Programming\\Workspaces\\BitBucket\\teethcart\\src\\icons\\icon_img.png");
        ImageIcon icon_star = createImageIcon("src\\icons\\icon_star.png");
        ImageIcon icon_stream = createImageIcon("src\\icons\\icon_stream.png");
 
        button_refreshImage = new JButton("Refresh", icon_img);
        button_refreshImage.setVerticalTextPosition(AbstractButton.CENTER);
        button_refreshImage.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
        //button_refreshImage.setMnemonic(KeyEvent.VK_D);
        button_refreshImage.setActionCommand("refresh");
        button_refreshImage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if ("refresh".equals(e.getActionCommand())) {
					
					// _TEST Refreshing the current image
					// refresh the current image?
					System.out.println("Refreshing the source image!");
					/*
					val_detectThreshold = 10f;
					slider_detectThreshold.setValue((int) val_detectThreshold);
					val_extractRadius = 2;
					slider_extractRadius.setValue(val_extractRadius);
					val_maxFeaturesPerScale = 100;
					slider_maxFeaturesPerScale.setValue(val_maxFeaturesPerScale);
					val_initialSampleSize = 2;
					slider_initialSampleSize.setValue(val_initialSampleSize);
					val_initialSize = 9;
					slider_initialSize.setValue(val_initialSize);
					val_numberScalesPerOctave = 3;
					slider_numberScalesPerOctave.setValue(val_numberScalesPerOctave);
					val_numberOfOctaves = 4;					
					slider_numberOfOctaves.setValue(val_numberOfOctaves);
					*/
					refreshPreview();
					
		        } else {
		        	// do nothing?
		        }
				
			}
		});

	}

	protected void refreshPreview() {
		
		imagePanel = new ImagePanel(sourceImageBuffer);
		sourceImage = sourceImageBuffer;
		
		imagePanel.repaint();
		GUI.repaint();
	}

	@Override
	public void run() {

		imagePanel = new ImagePanel(sourceImage);

		workerImage = sourceImage;
		
		GUI.add(slider_detectThreshold);
		GUI.add(imagePanel, "span 5 5, wrap");
		GUI.add(slider_extractRadius, "wrap");
		GUI.add(slider_maxFeaturesPerScale, "wrap");
		GUI.add(slider_initialSampleSize, "wrap");
		GUI.add(slider_initialSize, "wrap");
		GUI.add(slider_numberScalesPerOctave, "wrap");
		GUI.add(slider_numberOfOctaves, "wrap");
		GUI.add(button_refreshImage, "wrap");

		GUI.pack();
		GUI.setVisible(true);

		//DetectFeatures(this.sourceImage, GrayF32.class);
	}

	public static <T extends ImageGray<T>> void DetectFeatures(BufferedImage image, Class<T> imageType) {
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType);

		// Create a Fast Hessian detector from the SURF paper.
		// Other detectors can be used in this example too.

		/*
		 * NOTEME ConfigFastHessian SURF
		 * 
		 * 
		 * float detectThreshold, Minimum feature intensity int extractRadius, Radius
		 * used for non-max-suppression (NMS) int maxFeaturesPerScale, Max num of
		 * features that NMS can return from each scale int initialSampleSize, How often
		 * pixels are sampled in the first octave int initialSize, Typically 9 int
		 * numberScalesPerOctave, Typically 4 int numberOfOctaves Typically 4
		 */
		//InterestPointDetector<T> detector = FactoryInterestPoint
		//		.fastHessian(new ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));

		// _FIX I thought this was addressed somewhere, I forget though...
		detector = new FactoryInterestPoint().fastHessian(new ConfigFastHessian(
						val_detectThreshold, 
						val_extractRadius, 
						val_maxFeaturesPerScale, 
						val_initialSampleSize, 
						val_initialSize, 
						val_numberScalesPerOctave, 
						val_numberOfOctaves));
		
		// find interest points in the image
		detector.detect(input);

		// Show the features
		displayResults(image, detector);
	}

	private static <T extends ImageGray<T>> void displayResults(BufferedImage image,
			InterestPointDetector<T> detector) {
		
		g2 = image.createGraphics();
		render = new FancyInterestPointRender();
		
		
		
		for (int i = 0; i < detector.getNumberOfFeatures(); i++) {
			Point2D_F64 pt = detector.getLocation(i);

			// note how it checks the capabilities of the detector
			if (detector.hasScale()) {
				int radius = (int) (detector.getRadius(i));
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}
		}
		// make the circle's thicker
		g2.setStroke(new BasicStroke(2));

		// just draw the features onto the input image
		render.draw(g2);
		

		// _TEST Removing the output for the testing phase of the UI
		// Output the beautiful detected Points Of Interest
		// Runnable r = new SaveBIOutput(image, "features");
		// new Thread(r).start();
		// MEMBERRY false will not close the terminate on window close
		// ShowImages.showWindow(image, "Detected Features", false);
		
		// imagePanel = new ImagePanel(image);
		// imagePanel.repaint();
		
		//imagePanel = new ImagePanel(workerBI);
		imagePanel = new ImagePanel(image);
		imagePanel.repaint();
		GUI.repaint();
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = Tailor_SURF.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
	
}
