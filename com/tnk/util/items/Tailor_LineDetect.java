package com.tnk.util.items;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Tailor_LineDetect extends ImageTailor {

	private String opName = "LineDetect";
	private BufferedImage sourceImage;
	private float attribute = 0;
	private static List<BufferedImage> outputImages = new ArrayList<>();
	/**
	 * Operation
	 * 
	 * Depending on the operation, there can be multiple attributes
	 * 
	 * @param attribute
	 */
	
	// constructor
	public Tailor_LineDetect( float attribute, // this is where we can have the triggers to execute differ 
								 BufferedImage inputBI) {
		super(inputBI);
		this.attribute = attribute;
		this.sourceImage = inputBI;
	}
	
	@Override
	public void run() {
		outputImages.add(sourceImage);
	}
	
	public String getOpName() {
		return this.opName;
	}
	
}
