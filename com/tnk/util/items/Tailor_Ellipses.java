package com.tnk.util.items;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.tnk.layout.Layout_FirstScreen;
import com.tnk.util.SaveBIOutput;
import com.tnk.util.Utilities;

import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.Contour;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.alg.shapes.FitData;
import boofcv.alg.shapes.ShapeFittingOps;
import boofcv.gui.binary.VisualizeBinaryData;
import boofcv.gui.feature.VisualizeShapes;
import boofcv.gui.image.ShowImages;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.ConnectRule;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayU8;
import georegression.struct.shapes.EllipseRotated_F64;


public class Tailor_Ellipses extends ImageTailor {

	private static List<BufferedImage> outputImages = new ArrayList<>();
	
	private String opName = "Ellipses";
	
	private BufferedImage inputImage;
	private float strokeWeight = 3.0f;
	
	/**
	 * Operation
	 * 
	 * Depending on the operation, there can be multiple attributes
	 * 
	 * @param attribute
	 */
	
	// constructors
	
	public Tailor_Ellipses( float strokeWeight, 			// attribute 1
			 					BufferedImage inputBI	// image to play with
			 					) 
	{
		super(inputBI);
		this.strokeWeight = strokeWeight;
		this.inputImage = inputBI;
	}
	public Tailor_Ellipses( float strokeWeight, 
								String inputPath) 
	{
		super(inputPath);
		this.strokeWeight = strokeWeight;
		this.inputImage = UtilImageIO.loadImage(inputPath);
	}
	
	@Override
	public void run() {
		
		GrayF32 input = ConvertBufferedImage.convertFromSingle(this.inputImage, 
																null, 
																GrayF32.class);

		GrayU8 binary = new GrayU8(input.width, input.height);

		// the mean pixel value is often a reasonable threshold when creating a binary
		// image
		double mean = ImageStatistics.mean(input);

		// create a binary image by thresholding
		ThresholdImageOps.threshold(input, binary, (float) mean, true);

		// reduce noise with some filtering
		GrayU8 filtered = BinaryImageOps.erode8(binary, 1, null);
		filtered = BinaryImageOps.dilate8(filtered, 1, null);
		
		// MEMBERRY This maybe be applicable to a grayscale of the superpixels output!?
		// Find the contour around the shapes
		List<Contour> contours = BinaryImageOps.contour(filtered, ConnectRule.EIGHT, null);

		// Fit an ellipse to each external contour and draw the results
		Graphics2D g2 = inputImage.createGraphics();
		g2.setStroke(new BasicStroke(this.strokeWeight));
		g2.setColor(Color.RED);

		for (Contour c : contours) {
			FitData<EllipseRotated_F64> ellipse = ShapeFittingOps.fitEllipse_I32(c.external, 0, false, null);
			VisualizeShapes.drawEllipse(ellipse.shape, g2);
		}
		
		// LABEL Ellipses Output
		// __ISSUE FIXED #013 [TASK] Implement a debugMode check
		
		if (Utilities.debugMode) {
		Runnable outBinary = new SaveBIOutput(VisualizeBinaryData.renderBinary(filtered, false, null),"Binary");
		new Thread(outBinary).start();
		
		Runnable outEllipses = new SaveBIOutput(inputImage, "FitEllipses_output");
		new Thread(outEllipses).start();
		
		ShowImages.showWindow(VisualizeBinaryData.renderBinary(filtered, false, null), "Binary", false);
		ShowImages.showWindow(inputImage, "Ellipses", false);
		}
		
		outputImages.add(inputImage);
		outputImages.add(VisualizeBinaryData.renderBinary(filtered, false, null));
		// _FIX Calling the output method from a RUNNABLE does not seem to work
		setOutput(outputImages);
	}
	
	/**
	 * LABEL     Proper way to send an output image to the main application
	 * MEMBERRY	 Call this once the thread containing this runnable has been run();
	 * 
	 * Retrieve the output of the operation once the Thread is run
	 * @return 
	 */
	public List<BufferedImage> getOutput() {
		return outputImages;
	}
	
	/**
	 * Get the Name of the Operation - Display Purposes...Probably
	 * @return
	 */
	public String getOpName() {
		return this.opName;
	}
	
	public void setOutput(List<BufferedImage> output) {
		Layout_FirstScreen.SetInProcessImage(output);
	}
	
}
