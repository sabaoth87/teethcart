package com.tnk.util.impro;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JSlider;

import com.tnk.util.SaveBIOutput;

import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.abst.feature.detect.interest.InterestPointDetector;
import boofcv.factory.feature.detect.interest.FactoryInterestPoint;
import boofcv.gui.feature.FancyInterestPointRender;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.ImageGray;
import georegression.struct.point.Point2D_F64;
import net.miginfocom.swing.MigLayout;

public class DetectFeaturesRunnable implements Runnable {

	private String imagePath;
	public JSlider slider_detectThreshold;
	public JSlider slider_extractRadius;
	public JSlider slider_maxFeaturesPerScale;
	public JSlider slider_initialSampleSize;
	public JSlider slider_initialSize;
	public JSlider slider_numberScalesPerOctave;
	public JSlider slider_numberOfOctaves;
	
	/*
	 * NOTEME Default values based on
	 * ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));
	 */
	
	public float val_detectThreshold = 10f;
	public int val_extractRadius = 2;
	public int val_maxFeaturesPerScale = 100;
	public int val_initialSampleSize = 2;
	public int val_initialSize = 9;
	public int val_numberScalesPerOctave = 3;
	public int val_numberOfOctaves = 4;
	
	public JFrame GUI;

	public DetectFeaturesRunnable(String selectedPath) {
		
		imagePath = selectedPath;
		
		
		initiailizeGUI();
		
		
	}

	private void initiailizeGUI() {
		
		GUI = new JFrame();
		//GUI.setBounds(0, 0, 1366, 768); // for my small ass monitor :'(
		GUI.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI.getContentPane().setLayout(new MigLayout(" ", "[fill][grow][grow]",
														   "[grow][fill][fill]"));
		
		
		slider_detectThreshold = new JSlider();
		slider_detectThreshold.setMaximum(20); slider_detectThreshold.setMinimum(0);
		slider_detectThreshold.setValue(10);
		val_detectThreshold = Float.intBitsToFloat(slider_detectThreshold.getValue());
		
		slider_extractRadius = new JSlider();
		slider_extractRadius.setMaximum(10); slider_extractRadius.setMinimum(0);
		slider_extractRadius.setValue(2);
		val_extractRadius = slider_extractRadius.getValue();
		
		slider_maxFeaturesPerScale = new JSlider();
		slider_maxFeaturesPerScale.setMaximum(200); slider_maxFeaturesPerScale.setMinimum(0);
		slider_maxFeaturesPerScale.setValue(100);
		val_maxFeaturesPerScale = slider_maxFeaturesPerScale.getValue();
		
		slider_initialSampleSize = new JSlider();
		slider_initialSampleSize.setMaximum(5); slider_initialSampleSize.setMinimum(0);
		slider_initialSampleSize.setValue(2);
		val_initialSampleSize = slider_initialSampleSize.getValue();
		
		slider_initialSize = new JSlider();
		slider_initialSize.setMaximum(20); slider_initialSize.setMinimum(0);
		slider_initialSize.setValue(9);
		val_initialSize = slider_initialSize.getValue();
		
		slider_numberScalesPerOctave = new JSlider();
		slider_numberScalesPerOctave.setMaximum(10); slider_numberScalesPerOctave.setMinimum(0);
		slider_numberScalesPerOctave.setValue(4);
		val_numberScalesPerOctave = slider_numberScalesPerOctave.getValue();

		slider_numberOfOctaves = new JSlider();
		slider_numberOfOctaves.setMaximum(10); slider_numberOfOctaves.setMinimum(0);
		slider_numberOfOctaves.setValue(4);
		val_numberOfOctaves = slider_numberOfOctaves.getValue();
		
	}

	@Override
	public void run() {
		BufferedImage bi = UtilImageIO.loadImage(imagePath);
		
		ImagePanel imagePanel = new ImagePanel(bi);
		
		
		GUI.add(slider_detectThreshold);
		GUI.add(imagePanel, "span 3 3, wrap");
		GUI.add(slider_extractRadius);
		GUI.add(slider_maxFeaturesPerScale);
		GUI.add(slider_initialSampleSize);
		GUI.add(slider_initialSize);
		GUI.add(slider_numberScalesPerOctave);
		GUI.add(slider_numberOfOctaves);
		
		
		GUI.pack();
		GUI.setVisible(true);
		
		DetectFeatures(bi, GrayF32.class);
	}

	public static <T extends ImageGray<T>> void DetectFeatures(BufferedImage image, Class<T> imageType) {
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType);

		// Create a Fast Hessian detector from the SURF paper.
		// Other detectors can be used in this example too.

		/*
		 * NOTEME ConfigFastHessian SURF
		 * 
		 * 
		 * float detectThreshold, 			Minimum feature intensity
		 * int extractRadius, 				Radius used for non-max-suppression (NMS)
		 * int maxFeaturesPerScale, 		Max num of features that NMS can return from each scale
		 * int initialSampleSize, 			How often pixels are sampled in the first octave
		 * int initialSize, 				Typically 9
		 * int numberScalesPerOctave, 		Typically 4
		 * int numberOfOctaves				Typically 4
		 */
		InterestPointDetector<T> detector = FactoryInterestPoint
				.fastHessian(new ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));

		// find interest points in the image
		detector.detect(input);

		// Show the features
		displayResults(image, detector);
	}

	private static <T extends ImageGray<T>> void displayResults(BufferedImage image,
			InterestPointDetector<T> detector) {
		Graphics2D g2 = image.createGraphics();
		FancyInterestPointRender render = new FancyInterestPointRender();

		for (int i = 0; i < detector.getNumberOfFeatures(); i++) {
			Point2D_F64 pt = detector.getLocation(i);

			// note how it checks the capabilities of the detector
			if (detector.hasScale()) {
				int radius = (int) (detector.getRadius(i));
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}
		}
		// make the circle's thicker
		g2.setStroke(new BasicStroke(3));

		// just draw the features onto the input image
		render.draw(g2);

		// Output the beautiful detected Points Of Interest
		Runnable r = new SaveBIOutput(image, "features");
		new Thread(r).start();
		// MEMBERRY false will not close the terminate on window close
		ShowImages.showWindow(image, "Detected Features", false);
	}

}
