package com.tnk.util.impro;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.List;

import com.tnk.util.SaveBIOutput;

import boofcv.abst.feature.detect.line.DetectLineHoughFoot;
import boofcv.abst.feature.detect.line.DetectLineHoughFootSubimage;
import boofcv.abst.feature.detect.line.DetectLineHoughPolar;
import boofcv.factory.feature.detect.line.ConfigHoughFoot;
import boofcv.factory.feature.detect.line.ConfigHoughFootSubimage;
import boofcv.factory.feature.detect.line.ConfigHoughPolar;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.gui.feature.ImageLinePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayS16;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.ImageGray;
import georegression.struct.line.LineParametric2D_F32;

public class LineDetectRunnable implements Runnable {

	private String imagePath;
	/*
	 * Adjusts edge threshold for identifying pixels belonging to a line
	 */
	private static final float edgeThreshold = 25;
	/**
	 * Adjust the maximum number of found lines in an image
	 */
	private static final int maxLines = 10;

	public LineDetectRunnable(String selectedPath) {
		imagePath = selectedPath;
	}

	@Override
	public void run() {
		LineDetect(imagePath, GrayU8.class, GrayS16.class, 7);
	}

	public static <T extends ImageGray<T>, D extends ImageGray<D>> void LineDetect(String imagePath, Class<T> imageType,
			Class<D> derivType, int numberOfLines) {

		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType);

		DetectLineHoughPolar<T, D> detectorPolar = FactoryDetectLineAlgs.houghPolar(
				new ConfigHoughPolar(3, 30, 2, Math.PI / 180, edgeThreshold, numberOfLines), imageType, derivType);
		List<LineParametric2D_F32> foundPolar = detectorPolar.detect(input);

		DetectLineHoughFoot<T, D> detectorFoot = FactoryDetectLineAlgs
				.houghFoot(new ConfigHoughFoot(3, 8, 5, edgeThreshold, maxLines), imageType, derivType);
		List<LineParametric2D_F32> foundFoot = detectorFoot.detect(input);

		DetectLineHoughFootSubimage<T, D> detectorFootsubimage = FactoryDetectLineAlgs.houghFootSub(
				new ConfigHoughFootSubimage(3, 8, 5, edgeThreshold, maxLines, 2, 2), imageType, derivType);
		List<LineParametric2D_F32> foundFootsub = detectorFootsubimage.detect(input);

		ImageLinePanel gui = new ImageLinePanel();
		gui.setBackground(image);
		gui.setLines(foundPolar);
		gui.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		// ShowImages.showWindow(VisualizeBinaryData.renderBinary(filtered, false,
		// null),"Binary",true);

		// Output the beautiful detected Points Of Interest
		Runnable r = new SaveBIOutput(image, "1inez");
		new Thread(r).start();

		ShowImages.showWindow(gui, "LINEZ@@", true);

	}

}
