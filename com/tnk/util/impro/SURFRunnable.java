package com.tnk.util.impro;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JSlider;

import com.tnk.util.FeatureSet;
import com.tnk.util.FeaturesHandler;
import com.tnk.util.Utilities;

import boofcv.abst.feature.detdesc.DetectDescribePoint;
import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.factory.feature.detdesc.FactoryDetectDescribe;
import boofcv.gui.feature.FancyInterestPointRender;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.feature.BrightFeature;
import boofcv.struct.image.GrayF32;
import georegression.struct.point.Point2D_F64;
import net.miginfocom.swing.MigLayout;

public class SURFRunnable implements Runnable {

	// I thought I would always have to use this
	// but if I just operate the I/O with BufferedImages
	// I should need to worry about where it came from
	private String imagePath;

	private BufferedImage inputBI;


	public JSlider slider_detectThreshold;
	public JSlider slider_extractRadius;
	public JSlider slider_maxFeaturesPerScale;
	public JSlider slider_initialSampleSize;
	public JSlider slider_initialSize;
	public JSlider slider_numberScalesPerOctave;
	public JSlider slider_numberOfOctaves;
	
	/*
	 * NOTEME Default values based on
	 * ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));
	 */
	
	public float val_detectThreshold = 10f;
	public int val_extractRadius = 2;
	public int val_maxFeaturesPerScale = 100;
	public int val_initialSampleSize = 2;
	public int val_initialSize = 9;
	public int val_numberScalesPerOctave = 3;
	public int val_numberOfOctaves = 4;
	
	public JFrame GUI;

	
	private static FeaturesHandler fHandler;
	private static FeaturesHandler fHandlerMax;

	private static FeatureSet[] surfFeatures;
	private static FeatureSet[] surfFeaturesMax;

	public SURFRunnable(BufferedImage inputImage) {
		inputBI = inputImage;
		
		initiailizeGUI();
		
	}
	

	private void initiailizeGUI() {
		
		GUI = new JFrame();
		//GUI.setBounds(0, 0, 1366, 768); // for my small ass monitor :'(
		GUI.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GUI.getContentPane().setLayout(new MigLayout(" ", "[fill][grow][grow]",
														   "[grow][fill][fill]"));
		
		
		slider_detectThreshold = new JSlider();
		slider_detectThreshold.setMaximum(20); slider_detectThreshold.setMinimum(0);
		slider_detectThreshold.setValue(10);
		val_detectThreshold = Float.intBitsToFloat(slider_detectThreshold.getValue());
		
		slider_extractRadius = new JSlider();
		slider_extractRadius.setMaximum(10); slider_extractRadius.setMinimum(0);
		slider_extractRadius.setValue(2);
		val_extractRadius = slider_extractRadius.getValue();
		
		slider_maxFeaturesPerScale = new JSlider();
		slider_maxFeaturesPerScale.setMaximum(200); slider_maxFeaturesPerScale.setMinimum(0);
		slider_maxFeaturesPerScale.setValue(100);
		val_maxFeaturesPerScale = slider_maxFeaturesPerScale.getValue();
		
		slider_initialSampleSize = new JSlider();
		slider_initialSampleSize.setMaximum(5); slider_initialSampleSize.setMinimum(0);
		slider_initialSampleSize.setValue(2);
		val_initialSampleSize = slider_initialSampleSize.getValue();
		
		slider_initialSize = new JSlider();
		slider_initialSize.setMaximum(20); slider_initialSize.setMinimum(0);
		slider_initialSize.setValue(9);
		val_initialSize = slider_initialSize.getValue();
		
		slider_numberScalesPerOctave = new JSlider();
		slider_numberScalesPerOctave.setMaximum(10); slider_numberScalesPerOctave.setMinimum(0);
		slider_numberScalesPerOctave.setValue(4);
		val_numberScalesPerOctave = slider_numberScalesPerOctave.getValue();

		slider_numberOfOctaves = new JSlider();
		slider_numberOfOctaves.setMaximum(10); slider_numberOfOctaves.setMinimum(0);
		slider_numberOfOctaves.setValue(4);
		val_numberOfOctaves = slider_numberOfOctaves.getValue();
		
	}

	

	@Override
	public void run() {
		// create the detector and descriptors
		BufferedImage image = inputBI;
		GrayF32 input = ConvertBufferedImage.convertFromSingle(image, null, GrayF32.class);
		DetectDescribePoint<GrayF32, BrightFeature> surf = FactoryDetectDescribe
				.surfStable(new ConfigFastHessian(0, 2, 200, 2, 9, 4, 4), null, null, GrayF32.class);

		// specify the image to process
		surf.detect(input);

		BrightFeature feature00 = surf.getDescription(0);
		System.out.println("Found Features: " + surf.getNumberOfFeatures());
		System.out.println("First descriptor's first value: " + surf.getDescription(0).value[0]);
		Utilities.DW_AddColouredText("Found " + surf.getNumberOfFeatures(), Color.ORANGE);
		Utilities.DW_AddColouredText("[0]location " + surf.getLocation(0), Color.GREEN);
		Utilities.DW_AddColouredText("[1]location " + surf.getLocation(1), Color.GREEN);
		Utilities.DW_AddColouredText("[0]desc.double " + surf.getRadius(0), Color.MAGENTA);

		double[] radii = new double[surf.getNumberOfFeatures()];
		Point2D_F64[] locations = new Point2D_F64[surf.getNumberOfFeatures()];
		FeatureSet[] featureSet = new FeatureSet[surf.getNumberOfFeatures()]; // FeatureSet for ALL of the features
		int setSize = surf.getNumberOfFeatures();
		fHandler = new FeaturesHandler(featureSet, setSize);

		for (int x = 0; x < surf.getNumberOfFeatures(); x++) {
			locations[x] = surf.getLocation(x);
			radii[x] = surf.getRadius(x);

			// new approach
			System.out.println("Adding " + x + " feature");
			fHandler.addFeature(radii[x], locations[x]);
		}

		surfFeatures = fHandler.getFeatures();

		System.out.println("0 - " + surfFeatures[0].getRadius() + " " + surfFeatures[0].getLocation() + "\n1 - "
				+ surfFeatures[1].getRadius() + " " + surfFeatures[1].getLocation());

		/**
		 * _TEST SURF TESTING 
		 * 
		 * These are the test variables for my SURF work that will find the largest
		 * feature(s) in the image. Should have a min/max radius as well as the max
		 * amount of features returned - maybe based on total found?
		 */

		int maxFeaturesReturned = 5; // represents size of returned array[s]

		int whileRun = 0; // while loop holder - ends when == maxFeaturesReturned
		int index = 0;

		double[] maxRad = new double[maxFeaturesReturned]; // array of big radii
		Point2D_F64[] maxRadLocs = new Point2D_F64[maxFeaturesReturned]; // array of locations
		FeatureSet[] maxFeatureSet = new FeatureSet[maxFeaturesReturned];
		fHandlerMax = new FeaturesHandler(maxFeatureSet, maxFeaturesReturned);

		Arrays.sort(radii);
		System.out.println("Radii array size : " + radii.length);
		System.out.println(radii[surf.getNumberOfFeatures() - 1]);
		for (int flow = 0; flow < maxFeaturesReturned; flow++) {
			maxRad[flow] = radii[(surf.getNumberOfFeatures() - 1) - flow];
			maxRadLocs[flow] = locations[(surf.getNumberOfFeatures() - 1) - flow];

			System.out.println("Adding " + flow + " of " + maxFeaturesReturned);

			fHandlerMax.addFeature(radii[(surf.getNumberOfFeatures() - 1) - flow],
					locations[(surf.getNumberOfFeatures() - 1) - flow]);
		}

		Graphics2D g2 = image.createGraphics();
		FancyInterestPointRender render = new FancyInterestPointRender();

		for (int dbg = 0; dbg <= (maxFeaturesReturned - 1); dbg++) {
			System.out.println("SURFing ::=:: " + maxRad[dbg] + " " + maxRadLocs[dbg]);
		}

		for (int i = 1; i < maxFeaturesReturned; i++) {

			Point2D_F64 pt = maxRadLocs[i];

			if (surf.hasScale()) {
				int radius = (int) maxRad[i];
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}

		}
		surfFeaturesMax = fHandlerMax.getFeatures();
		// make the circle's thicker
		g2.setStroke(new BasicStroke(3));

		// just draw the features onto the input image
		render.draw(g2);
		ShowImages.showWindow(image, "Detected Features", false);

		System.out.println("Largest Radii: " + surf.getRadius(index));
		System.out.println("Located At: " + surf.getLocation(index));

		System.out.println("Your Feature Set now holds " + surfFeatures.length + " entries. First "
				+ maxFeaturesReturned + " entries:");
		for (int count = 0; count < maxFeaturesReturned; count++) {
			System.out.println(" Feature[" + count + "] : rad : " + surfFeatures[count].getRadius() + " locale : "
					+ surfFeatures[count].getLocation());
		}
		System.out.println("Your POI Feature Set now holds " + maxFeaturesReturned + " entries");
		for (int count = 0; count < maxFeaturesReturned; count++) {
			System.out.println(" Feature[" + count + "] : rad : " + surfFeaturesMax[count].getRadius() + " locale : "
					+ surfFeaturesMax[count].getLocation());
		}

	}

}
