package com.tnk.util.impro;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import com.tnk.layout.Layout_FirstScreen;
import com.tnk.util.SaveBIOutput;

import boofcv.alg.color.ColorHsv;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;

public class ColourSegmentRunnable implements Runnable {

	/*
	 * // Use this for 'volatile field update' private volatile boolean canceled =
	 * false; public void cancelThread() { this.canceled = true; }
	 */

	// I think I'm always going to need this one
	private String imagePath;

	private int progressCount = 0;
	

	
	public ColourSegmentRunnable(String selectedPath) {
		this.imagePath = selectedPath;
	}

	@Override
	public void run() {
		printClickedColour(imagePath);
	}

	public static void printClickedColour(final String imagePath) {
		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));
		// __ISSUE #008 [ENHANCE] Need to set restraints on the Colour Picker Size

		ImagePanel gui = new ImagePanel(image);
		gui.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (SwingUtilities.isLeftMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					/**
					 * MEMBERRY use this to grow an array of values to use for further segmentation
					 * effort. I want to be able to quickly 'teach' the program by clicking a batch
					 * of sample swatches that can be used by averaging out their value and
					 * segmenting similar areas. ie Click a couple shades of brown, it finds
					 * brown-ish ares in an image. I think this could be a stepping stone to
					 * teaching full blown features
					 */
					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					
					
					showSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);

					
				}

				if (SwingUtilities.isRightMouseButton(e)) {

					float[] colour = new float[3];
					int rgb = image.getRGB(e.getX(), e.getY());

					ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
					System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);
					// Output the remains
					Runnable r = new SaveBIOutput(image, "colseg_H-"+colour[0] + "S-" + colour[1]+"V-" + colour[2]);
					new Thread(r).start();
					removeSelectedColour("hue:" + colour[0] + " sat: " + colour[1], image, colour[0], colour[1]);
					
				}

			}
		});
		ShowImages.showWindow(gui, "Click a colour area!", false);
	}

	/**
	 * Selectively displays only pixels which have a similar hue and saturation
	 * values to what is provided. This is intended to be a simple example of colour
	 * based segmentation. Colour based segmentation can be done in RGB colour, but
	 * more problematic due to it not being intensity invariant. More robust
	 * techniques can use Gaussian models instead of a uniform distribution, as is
	 * done below.
	 *
	 * @param name
	 * @param image
	 * @param hue
	 * @param saturation
	 */
	public static void showSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;

				 //LABEL colour removing
				 if( dist2 <= maxDist2 ) {
				 output.setRGB(x,y,image.getRGB(x,y));
				 }

			}
		}

		ShowImages.showWindow(output, "Showing " + name);

	}

	public static void removeSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;


				// LABEL colour excluding?
				if (maxDist2 <= dist2) {
					output.setRGB(x, y, image.getRGB(x, y));
				}
			}
		}

		ShowImages.showWindow(output, "Showing " + name);

	}
}
