package com.tnk.util;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.jws.soap.SOAPBinding.Style;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.ddogleg.struct.FastQueue;
import org.ddogleg.struct.GrowQueue_I32;

import com.sun.jna.platform.unix.X11.Font;
import com.tnk.Item_OperationInput;

import boofcv.abst.feature.detdesc.DetectDescribePoint;
import boofcv.abst.feature.detect.interest.ConfigFastHessian;
import boofcv.abst.feature.detect.interest.InterestPointDetector;
import boofcv.abst.feature.detect.line.DetectLineHoughFoot;
import boofcv.abst.feature.detect.line.DetectLineHoughFootSubimage;
import boofcv.abst.feature.detect.line.DetectLineHoughPolar;
import boofcv.abst.filter.derivative.AnyImageDerivative;
import boofcv.abst.segmentation.ImageSuperpixels;
import boofcv.alg.color.ColorHsv;
import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.Contour;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.filter.blur.GBlurImageOps;
import boofcv.alg.filter.derivative.DerivativeType;
import boofcv.alg.filter.derivative.GImageDerivativeOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.alg.segmentation.ComputeRegionMeanColor;
import boofcv.alg.segmentation.ImageSegmentationOps;
import boofcv.alg.shapes.FitData;
import boofcv.alg.shapes.ShapeFittingOps;
import boofcv.core.image.border.BorderType;
import boofcv.factory.feature.detdesc.FactoryDetectDescribe;
import boofcv.factory.feature.detect.interest.FactoryInterestPoint;
import boofcv.factory.feature.detect.line.ConfigHoughFoot;
import boofcv.factory.feature.detect.line.ConfigHoughFootSubimage;
import boofcv.factory.feature.detect.line.ConfigHoughPolar;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.factory.segmentation.ConfigFh04;
import boofcv.factory.segmentation.FactoryImageSegmentation;
import boofcv.factory.segmentation.FactorySegmentationAlg;
import boofcv.gui.ListDisplayPanel;
import boofcv.gui.binary.VisualizeBinaryData;
import boofcv.gui.feature.FancyInterestPointRender;
import boofcv.gui.feature.ImageLinePanel;
import boofcv.gui.feature.VisualizeRegions;
import boofcv.gui.feature.VisualizeShapes;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.gui.image.VisualizeImageData;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.ConnectRule;
import boofcv.struct.feature.BrightFeature;
import boofcv.struct.feature.ColorQueue_F32;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayS16;
import boofcv.struct.image.GrayS32;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.ImageBase;
import boofcv.struct.image.ImageGray;
import boofcv.struct.image.ImageType;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;
import georegression.struct.line.LineParametric2D_F32;
import georegression.struct.point.Point2D_F64;
import georegression.struct.shapes.EllipseRotated_F64;
import net.miginfocom.swing.MigLayout;

/*
 * LABEL [UTILITIES]
 * 
 * This is my grab bag of utilities for the project
 */
/*
 * LABEL [LIST OF TASKS]
 * 
 * __ISSUE FIXED #006 [TASK] Separate out the IP Methods  
 */

public class Utilities {

	public static boolean debugMode = true;
	
	
	public BufferedImage imageInProcess;
	
	private static DefaultMutableTreeNode root;
	private static DefaultTreeModel treeModel;

	public static Item_OperationInput runLine;
	public Item_OperationInput runBlob;
	// private JTree tree;

	public static JRadioButton runLineRadio;
	public static JRadioButton runBlobRadio;
	public static JRadioButton runSURFRadio;
	public static JRadioButton runAIPRadio;
	public static JRadioButton runImageDerivRadio;
	public static JRadioButton runDIFRadio;
	public static JRadioButton runColourSegRadio;
	public static JRadioButton runSceneRecogRadio;

	public static JTextPane _window;
	

	/**
	 * Adjusts edge threshold for identifying pixels belonging to a line
	 */
	private static final float edgeThreshold = 25;
	/**
	 * Adjust the maximum number of found lines in an image
	 */
	private static final int maxLines = 10;
	private static ListDisplayPanel listPanel = new ListDisplayPanel();

	private static FeaturesHandler fHandler;
	private static FeaturesHandler fHandlerMax;
	
	private static FeatureSet[] surfFeatures;
	private static FeatureSet[] surfFeaturesMax;
	
	
	public Utilities() {
		
	}

	/**
	 * LABEL [DriveExplorer] Frankenstein'd version of a File Browser; SO:Java FTW
	 * !!
	 * 
	 * @param fileSystemView
	 * @return
	 */
	public static JTree DriveExplorer(FileSystemView fileSystemView) {
		// FileSystemView fileSystemView = FileSystemView.getFileSystemView();
		// File fileRoot = new File("C:/");
		// root = new DefaultMutableTreeNode(new FileNode(fileRoot));
		// treeModel = new DefaultTreeModel(root);

		// the File tree
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();
		treeModel = new DefaultTreeModel(root);
		// show the file system roots
		File[] roots = fileSystemView.getRoots();
		for (File fileSystemRoot : roots) {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileSystemRoot);
			root.add(node);
			File[] files = fileSystemView.getFiles(fileSystemRoot, true);
			for (File file : files) {
				if (file.isDirectory()) {
					node.add(new DefaultMutableTreeNode(file));
				}
			}
		}

		// Lets make the tree variable
		JTree tree = new JTree(treeModel);
		// Change some attributes
		tree.setRootVisible(false);
		// tree.addTreeSelectionListener(tsl);
		tree.setCellRenderer(new FileTreeCellRenderer());
		// tree.setShowsRootHandles(true);
		tree.setToolTipText("My Tree!");
		tree.expandRow(0);
		tree.setVisibleRowCount(15);
		return tree;
	}

	/**
	 * LABEL [IPa - Detect Features]
	 * Algorithm ready Detect Features method
	 * 
	 * Detects features
	 * 
	 * Outputs a .PNG of the found data "FeatureDetection_output");
	 * This now also returns a BufferedImage
	 * 
	 * 
	 * @param image
	 * @param imageType
	 * @return
	 */

	public static <T extends ImageGray<T>> BufferedImage IPa_DetectFeatures(BufferedImage image, Class<T> imageType) {
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType);

		// Create a Fast Hessian detector from the SURF paper.
		// __ISSUE #014 [ENHANCE] Look into other factory detectors that can be used in place of Fast Hessian
		InterestPointDetector<T> detector = FactoryInterestPoint
				.fastHessian(new ConfigFastHessian(10, 2, 100, 2, 9, 3, 4));

		// find interest points in the image
		detector.detect(input);

		// Show the features
		return displayResults(image, detector);
	}

	private static <T extends ImageGray<T>> BufferedImage displayResults(BufferedImage image,
			InterestPointDetector<T> detector) {
		Graphics2D g2 = image.createGraphics();
		FancyInterestPointRender render = new FancyInterestPointRender();

		for (int i = 0; i < detector.getNumberOfFeatures(); i++) {
			Point2D_F64 pt = detector.getLocation(i);

			// note how it checks the capabilities of the detector
			if (detector.hasScale()) {
				int radius = (int) (detector.getRadius(i));
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}
		}
		// make the circle's thicker
		g2.setStroke(new BasicStroke(3));

		// just draw the features onto the input image
		render.draw(g2);

		
		// LABEL IP_DetectFeatures Output
		
		System.out.println("IP_DetectFeatures found " + detector.getNumberOfFeatures() + " in the processed image!");
		
		// output the image with the detected features overlaid 
		IP_Util_SaveBI(image, "IPaFD");
		// display the output to the user
		ShowImages.showWindow(image, "Detected Features " + detector.getNumberOfFeatures(), false);
		// return the image with detected features to the caller
		return image;
	}

	/**
	 * LABEL [IP_Util_SaveBI] _CLEAN This has been deprecated! Remove? 
	 * Simple routine to simplify the output from all of these Image Processes during the
	 * testing phases(s)
	 * 
	 * @param image
	 * @param fileName
	 */
	public static void IP_Util_SaveBI(BufferedImage image, String fileName) {
		String TAG = "IP_Util_SaveBI ";
		System.out.println(TAG + "Saving an image to disk...");
		Date outputDate = new Date();
		String outputPath = new String("C:\\test\\output\\" + outputDate.getMonth() + "-" + outputDate.getDate() + " "
				+ outputDate.getHours() + "h" + outputDate.getMinutes() + "m " + fileName + ".png");
		File f = new File(outputPath);
		UtilImageIO.saveImage(image, outputPath);
		System.out.println(TAG + "Saved " + outputPath);
	}

	/**
	 * LABEL [IPa_LineDetect] 
	 * Algorithm ready 
	 * Find lines example from BoofCV - thanks!
	 * Outputs "LineDetection_output.PNG"
	 * 
	 * 
	 * @param imagePath
	 * @param imageType
	 * @param derivType
	 * @param numberOfLines
	 * @return BufferedImage
	 */
	public static <T extends ImageGray<T>, D extends ImageGray<D>> BufferedImage IPa_LineDetect(String imagePath,
			Class<T> imageType, Class<D> derivType, int numberOfLines) {

		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType);

		DetectLineHoughPolar<T, D> detectorPolar = FactoryDetectLineAlgs.houghPolar(
				new ConfigHoughPolar(3, 30, 2, Math.PI / 180, edgeThreshold, numberOfLines), imageType, derivType);
		List<LineParametric2D_F32> foundPolar = detectorPolar.detect(input);

		DetectLineHoughFoot<T, D> detectorFoot = FactoryDetectLineAlgs
				.houghFoot(new ConfigHoughFoot(3, 8, 5, edgeThreshold, maxLines), imageType, derivType);
		List<LineParametric2D_F32> foundFoot = detectorFoot.detect(input);

		DetectLineHoughFootSubimage<T, D> detectorFootsubimage = FactoryDetectLineAlgs.houghFootSub(
				new ConfigHoughFootSubimage(3, 8, 5, edgeThreshold, maxLines, 2, 2), imageType, derivType);
		List<LineParametric2D_F32> foundFootsub = detectorFootsubimage.detect(input);

		ImageLinePanel gui = new ImageLinePanel();
		gui.setBackground(image);
		gui.setLines(foundPolar);
		gui.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		// ShowImages.showWindow(VisualizeBinaryData.renderBinary(filtered, false,
		// null),"Binary",true);

		IP_Util_SaveBI(image, "LineDetection_output");

		
		ShowImages.showWindow(gui, "LINEZ@@", true);
		return image;
	}

	/**
	 * LABEL [IPa_FitEllipses] 
	 * Algorithm ready 
	 * Fit Ellipses example from BoofCV - thanks!!
	 * 
	 * Outputs "FitEllipses_output"
	 * 
	 * @param imagePath
	 * @return BufferedImage
	 */
	public static BufferedImage IPa_FitEllipses(String imagePath) {
		// load and convert the image into a usable format
		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));
		GrayF32 input = ConvertBufferedImage.convertFromSingle(image, null, GrayF32.class);

		GrayU8 binary = new GrayU8(input.width, input.height);

		// the mean pixel value is often a reasonable threshold when creating a binary
		// image
		double mean = ImageStatistics.mean(input);

		// create a binary image by thresholding
		ThresholdImageOps.threshold(input, binary, (float) mean, true);

		// reduce noise with some filtering
		GrayU8 filtered = BinaryImageOps.erode8(binary, 1, null);
		filtered = BinaryImageOps.dilate8(filtered, 1, null);

		// Find the contour around the shapes

		List<Contour> contours = BinaryImageOps.contour(filtered, ConnectRule.EIGHT, null);

		// Fit an ellipse to each external contour and draw the results
		Graphics2D g2 = image.createGraphics();
		g2.setStroke(new BasicStroke(3));
		g2.setColor(Color.RED);

		for (Contour c : contours) {
			FitData<EllipseRotated_F64> ellipse = ShapeFittingOps.fitEllipse_I32(c.external, 0, false, null);
			VisualizeShapes.drawEllipse(ellipse.shape, g2);
		}

		IP_Util_SaveBI(ConvertBufferedImage.convertTo(filtered, null), "FitEllipses_output");
		IP_Util_SaveBI(VisualizeBinaryData.renderBinary(filtered, false, null),"Binary");
		IP_Util_SaveBI(image, "FitEllipses_output");

		ShowImages.showWindow(VisualizeBinaryData.renderBinary(filtered, false, null), "Binary", false);
		ShowImages.showWindow(image, "Ellipses", false);
		
		return ConvertBufferedImage.convertTo(filtered, null);
	}

	/**
	 * LABEL [IP_EasySurf] Easy SURF example from BoofCV - thanks!!
	 * 
	 * @param imagePath
	 *            string path to the image
	 */
	public static void IP_EasySURF(String imagePath) {
		// create the detector and descriptors
		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));
		GrayF32 input = ConvertBufferedImage.convertFromSingle(image, null, GrayF32.class);
		DetectDescribePoint<GrayF32, BrightFeature> surf = FactoryDetectDescribe
				.surfStable(new ConfigFastHessian(0, 2, 200, 2, 9, 4, 4), null, null, GrayF32.class);

		// specify the image to process
		surf.detect(input);

		BrightFeature feature00 = surf.getDescription(0);
		// feature00.

		System.out.println("Found Features: " + surf.getNumberOfFeatures());
		System.out.println("First descriptor's first value: " + surf.getDescription(0).value[0]);
		Utilities.DW_AddColouredText("Found " + surf.getNumberOfFeatures(), Color.ORANGE);
		Utilities.DW_AddColouredText("[0]location " + surf.getLocation(0), Color.GREEN);
		Utilities.DW_AddColouredText("[1]location " + surf.getLocation(1), Color.GREEN);
		Utilities.DW_AddColouredText("[0]desc.double " + surf.getRadius(0), Color.MAGENTA);

		double[] radii = new double[surf.getNumberOfFeatures()];
		Point2D_F64[] locations = new Point2D_F64[surf.getNumberOfFeatures()];
		FeatureSet[] featureSet = new FeatureSet[surf.getNumberOfFeatures()];		//FeatureSet for ALL of the features
		int setSize = surf.getNumberOfFeatures();
		fHandler = new FeaturesHandler(featureSet, setSize);
		
		for (int x = 0; x < surf.getNumberOfFeatures(); x++) {
			locations[x] = surf.getLocation(x);
			radii[x] = surf.getRadius(x);
			
			
			// new approach
			System.out.println("Adding " + x + " feature");
			fHandler.addFeature( radii[x], locations[x]);
		}
		
		surfFeatures = fHandler.getFeatures();
		
		System.out.println("0 - " + surfFeatures[0].getRadius() + " " + surfFeatures[0].getLocation() +
							"\n1 - " + surfFeatures[1].getRadius() + " " + surfFeatures[1].getLocation());
		

		/**
		 * _TEST TESTING
		 * 
		 * These are the test variables for my SURF work that will find the largest
		 * feature(s) in the image. Should have a min/max radius as well as the max
		 * amount of features returned - maybe based on total found?
		 */

		int maxFeaturesReturned = 5; // represents size of returned array[s]

		int whileRun = 0; // while loop holder - ends when == maxFeaturesReturned
		int index = 0;
		
		double[] maxRad = new double[maxFeaturesReturned]; // array of big radii
		Point2D_F64[] maxRadLocs = new Point2D_F64[maxFeaturesReturned]; // array of locations
		FeatureSet[] maxFeatureSet = new FeatureSet[maxFeaturesReturned];
		fHandlerMax = new FeaturesHandler(maxFeatureSet, maxFeaturesReturned);

		Arrays.sort(radii);
		System.out.println("Radii array size : " + radii.length);
		System.out.println(radii[surf.getNumberOfFeatures()-1]);
		for (int flow = 0; flow < maxFeaturesReturned; flow++) {
			maxRad[flow] = radii[(surf.getNumberOfFeatures()-1) - flow];
			maxRadLocs[flow] = locations[(surf.getNumberOfFeatures()-1) - flow];
			
			System.out.println("Adding " + flow + " of " + maxFeaturesReturned);
			
			fHandlerMax.addFeature(radii[(surf.getNumberOfFeatures()-1) - flow], locations[(surf.getNumberOfFeatures()-1) - flow]);
		}
		
		Graphics2D g2 = image.createGraphics();
		FancyInterestPointRender render = new FancyInterestPointRender();

		for (int dbg = 0; dbg<=(maxFeaturesReturned-1);dbg++)
		{
			System.out.println("SURFing ::=:: " + maxRad[dbg] + " " + maxRadLocs[dbg] );
		}
		
		for (int i = 1; i < maxFeaturesReturned; i++) {

			Point2D_F64 pt = maxRadLocs[i];
			
			if (surf.hasScale()) {
				int radius = (int) maxRad[i];
				render.addCircle((int) pt.x, (int) pt.y, radius);
			} else {
				render.addPoint((int) pt.x, (int) pt.y);
			}

		}
		surfFeaturesMax = fHandlerMax.getFeatures();
		// make the circle's thicker
		g2.setStroke(new BasicStroke(3));

		// just draw the features onto the input image
		render.draw(g2);
		ShowImages.showWindow(image, "Detected Features", false);

		System.out.println("Largest Radii: " + surf.getRadius(index));
		System.out.println("Located At: " + surf.getLocation(index));
		
		System.out.println("Your Feature Set now holds " + surfFeatures.length + " entries. First " + maxFeaturesReturned + " entries:");
		for (int count = 0; count < maxFeaturesReturned; count++)
		{
			System.out.println(" Feature["+count+"] : rad : " + surfFeatures[count].getRadius() + " locale : " + surfFeatures[count].getLocation());
		}
		System.out.println("Your POI Feature Set now holds " + maxFeaturesReturned + " entries");
		for (int count = 0; count < maxFeaturesReturned; count++)
		{
			System.out.println(" Feature["+count+"] : rad : " + surfFeaturesMax[count].getRadius() + " locale : " + surfFeaturesMax[count].getLocation());
		}
	}


	/*
	 * Image Derivative
	 * 
	 * The gradient (1st order derivative) is probably the most important image
	 * derivative and is used as a first step when extracting many tyes of image
	 * features
	 * 
	 * @param String imagePath points to the image location
	 */
	public static void IP_ImageDerivative(String imagePath) {

		JFrame frame = new JFrame();
		frame.setBounds(0, 0, 300, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1));

		BufferedImage input = UtilImageIO.loadImage(imagePath);

		// We will use floating point images here, but GrayU8 with GrayS16 for
		// derivatives also works
		GrayF32 grey = new GrayF32(input.getWidth(), input.getHeight());
		ConvertBufferedImage.convertFrom(input, grey);

		// First order derivative, also known as the gradient
		GrayF32 derivX = new GrayF32(grey.width, grey.height);
		GrayF32 derivY = new GrayF32(grey.width, grey.height);

		GImageDerivativeOps.gradient(DerivativeType.SOBEL, grey, derivX, derivY, BorderType.EXTENDED);

		// Second order derivative, also known as the Hessian
		GrayF32 derivXX = new GrayF32(grey.width, grey.height);
		GrayF32 derivXY = new GrayF32(grey.width, grey.height);
		GrayF32 derivYY = new GrayF32(grey.width, grey.height);

		GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
				BorderType.EXTENDED);

		// There's also a built in function for computing arbitrary derivatives
		AnyImageDerivative<GrayF32, GrayF32> derivative = GImageDerivativeOps.createAnyDerivatives(DerivativeType.SOBEL,
				GrayF32.class, GrayF32.class);

		// the boolean sequence indicates if its an X or Y derivative
		derivative.setInput(grey);
		GrayF32 derivXYX = derivative.getDerivative(true, false, true);

		// visualize the results
		ListDisplayPanel gui = new ListDisplayPanel();

		gui.addImage(ConvertBufferedImage.convertTo(grey, null), "Input Grey");
		gui.addImage(VisualizeImageData.colorizeSign(derivX, null, -1), "Sobel X");
		gui.addImage(VisualizeImageData.colorizeSign(derivY, null, -1), "Sobel Y");

		// Use colours to show X and Y derivatives in one image
		// Looks pretty apparently?

		gui.addImage(VisualizeImageData.colorizeGradient(derivX, derivY, -1), "Sobel X and Y");
		gui.addImage(VisualizeImageData.colorizeSign(derivXX, null, -1), "Sobel XX");
		gui.addImage(VisualizeImageData.colorizeSign(derivXY, null, -1), "Sobel XY");
		gui.addImage(VisualizeImageData.colorizeSign(derivYY, null, -1), "Sobel YY");
		gui.addImage(VisualizeImageData.colorizeSign(derivXYX, null, -1), "Sobel XYX");

		// Output the results to disk

		File f = new File(imagePath);
		String outputPath = new String("C:\\test\\output\\output" + System.currentTimeMillis() + ".png");
		String outputName = new String(f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1) + "_"
				+ Long.toHexString(System.currentTimeMillis()) + "_");
		UtilImageIO.saveImage(derivX, outputPath);

		IP_Util_SaveBI(ConvertBufferedImage.convertTo(derivXYX, null), "DerivXYX_output");

		BufferedImage outputXYX = VisualizeImageData.colorizeSign(derivXYX, null, -1);
		IP_Util_SaveBI(outputXYX, outputName + "DerivXYX_output_colourized");

		BufferedImage outputX = VisualizeImageData.colorizeSign(derivX, null, -1);
		IP_Util_SaveBI(outputX, outputName + "DerivX_output_colourized");

		BufferedImage outputY = VisualizeImageData.colorizeSign(derivY, null, -1);
		IP_Util_SaveBI(outputY, outputName + "DerivY_output_colourized");

		BufferedImage outputXX = VisualizeImageData.colorizeSign(derivXX, null, -1);
		IP_Util_SaveBI(outputXX, outputName + "DerivXX_output_colourized");

		BufferedImage outputXY = VisualizeImageData.colorizeSign(derivXY, null, -1);
		IP_Util_SaveBI(outputXY, outputName + "DerivXX_output_colourized");

		BufferedImage outputYY = VisualizeImageData.colorizeSign(derivYY, null, -1);
		IP_Util_SaveBI(outputYY, outputName + "DerivYY_output_colourized");

		frame.add(gui);
		frame.setVisible(true);
		// ShowImages.showWindow(gui, "Image Derivatives", true);
	}

	public static BufferedImage IP_Algo_Deriv(String imagePath) {

		JFrame frame = new JFrame();
		frame.setBounds(0, 0, 300, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1));

		BufferedImage input = UtilImageIO.loadImage(imagePath);

		// We will use floating point images here, but GrayU8 with GrayS16 for
		// derivatives also works
		GrayF32 grey = new GrayF32(input.getWidth(), input.getHeight());
		ConvertBufferedImage.convertFrom(input, grey);

		// First order derivative, also known as the gradient
		GrayF32 derivX = new GrayF32(grey.width, grey.height);
		GrayF32 derivY = new GrayF32(grey.width, grey.height);

		GImageDerivativeOps.gradient(DerivativeType.SOBEL, grey, derivX, derivY, BorderType.EXTENDED);

		// Second order derivative, also known as the Hessian
		GrayF32 derivXX = new GrayF32(grey.width, grey.height);
		GrayF32 derivXY = new GrayF32(grey.width, grey.height);
		GrayF32 derivYY = new GrayF32(grey.width, grey.height);

		GImageDerivativeOps.hessian(DerivativeType.SOBEL, derivX, derivY, derivXX, derivXY, derivYY,
				BorderType.EXTENDED);

		// There's also a built in function for computing arbitrary derivatives
		AnyImageDerivative<GrayF32, GrayF32> derivative = GImageDerivativeOps.createAnyDerivatives(DerivativeType.SOBEL,
				GrayF32.class, GrayF32.class);

		// the boolean sequence indicates if its an X or Y derivative
		derivative.setInput(grey);
		GrayF32 derivXYX = derivative.getDerivative(true, false, true);

		// Output the results to disk
		File f = new File(imagePath);
		String outputPath = new String("C:\\test\\output\\output" + System.currentTimeMillis() + ".png");
		String outputName = new String(f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1) + "_"
				+ Long.toHexString(System.currentTimeMillis()) + "_");
		UtilImageIO.saveImage(derivX, outputPath);

		IP_Util_SaveBI(ConvertBufferedImage.convertTo(derivXYX, null), "DerivXYX_output");

		BufferedImage outputXYX = VisualizeImageData.colorizeSign(derivXYX, null, -1);
		IP_Util_SaveBI(outputXYX, outputName + "DerivXYX_output_colourized");

		BufferedImage outputX = VisualizeImageData.colorizeSign(derivX, null, -1);
		IP_Util_SaveBI(outputX, outputName + "DerivX_output_colourized");

		BufferedImage outputY = VisualizeImageData.colorizeSign(derivY, null, -1);
		IP_Util_SaveBI(outputY, outputName + "DerivY_output_colourized");

		BufferedImage outputXX = VisualizeImageData.colorizeSign(derivXX, null, -1);
		IP_Util_SaveBI(outputXX, outputName + "DerivXX_output_colourized");

		BufferedImage outputXY = VisualizeImageData.colorizeSign(derivXY, null, -1);
		IP_Util_SaveBI(outputXY, outputName + "DerivXX_output_colourized");

		BufferedImage outputYY = VisualizeImageData.colorizeSign(derivYY, null, -1);
		IP_Util_SaveBI(outputYY, outputName + "DerivYY_output_colourized");

		return VisualizeImageData.colorizeSign(derivXX, null, -1);
	}

	/**
	 * LABEL IP_DerivXYX 
	 * __ISSUE #005 [TASK] Separate all of the derivatives 
	 * 
	 * @param imagePath
	 * @return
	 */
	public GrayF32 IP_DerivXYX(String imagePath) {

		BufferedImage input = UtilImageIO.loadImage(imagePath);

		// We will use floating point images here, but GrayU8 with GrayS16 for
		// derivatives also works
		GrayF32 grey = new GrayF32(input.getWidth(), input.getHeight());

		// There's also a built in function for computing arbitrary derivatives
		AnyImageDerivative<GrayF32, GrayF32> derivative = GImageDerivativeOps.createAnyDerivatives(DerivativeType.SOBEL,
				GrayF32.class, GrayF32.class);

		// the boolean sequence indicates if its an X or Y derivative
		derivative.setInput(grey);
		GrayF32 derivXYX = derivative.getDerivative(true, false, true);

		return derivXYX;
	}

	public GrayF32 IP_DerivSobelX(String imagePath) {

		BufferedImage input = UtilImageIO.loadImage(imagePath);

		// We will use floating point images here, but GrayU8 with GrayS16 for
		// derivatives also works
		GrayF32 grey = new GrayF32(input.getWidth(), input.getHeight());

		ConvertBufferedImage.convertFrom(input, grey);

		// First order derivative, also known as the gradient
		GrayF32 derivX = new GrayF32(grey.width, grey.height);

		return derivX;
	}

	/**
	 * This method builds a simple JTextPane that can later be use by a manner of
	 * other methods, to be implemented.
	 * 
	 * @return _window - A Debugging readout in JTextPane form pretty much useless
	 *         without other methods such as:
	 * 
	 *         DW_AddColouredText(String text, Color colour)
	 *
	 */
	public static JTextPane DebugWindow() {

		_window = new JTextPane();
		_window.setPreferredSize(new Dimension(200, 200));

		return _window;
	}

	public static void DW_AddColouredText(String text, Color colour) {
		StyledDocument doc = _window.getStyledDocument();
		javax.swing.text.Style style = _window.addStyle("Color Style", null);
		StyleConstants.setForeground(style, colour);

		SimpleAttributeSet normal = new SimpleAttributeSet();
		StyleConstants.setFontFamily(normal, "SansSerif");
		StyleConstants.setForeground(style, colour);
		StyleConstants.setFontSize(normal, 12);

		SimpleAttributeSet boldBlue = new SimpleAttributeSet(normal);
		StyleConstants.setBold(boldBlue, true);
		StyleConstants.setFontSize(boldBlue, 15);
		StyleConstants.setForeground(boldBlue, Color.blue);

		SimpleAttributeSet highAlert = new SimpleAttributeSet(boldBlue);
		StyleConstants.setFontSize(highAlert, 18);
		StyleConstants.setItalic(highAlert, true);
		StyleConstants.setForeground(highAlert, Color.red);

		try {
			doc.insertString(doc.getLength(), "\n" + text, normal);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param imagePath
	 */
	public static void IP_ImageSegmentation(String imagePath) {
		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));

		// you probably don't want to segment along the image's alpha channel and the
		// code below assumes 3 channels
		image = ConvertBufferedImage.stripAlphaChannel(image);

		// Select input image type. Some algorithms behave different depending on image
		// type
		ImageType<Planar<GrayF32>> imageType = ImageType.pl(3, GrayF32.class);
		// ImageType<Planar<GrayU8>> imageType = ImageType.pl(3,GrayU8.class);
		// ImageType<GrayF32> imageType = ImageType.single(GrayF32.class);
		// ImageType<GrayU8> imageType = ImageType.single(GrayU8.class);

		// ImageSuperpixels alg = FactoryImageSegmentation.meanShift(null, imageType);
		// ImageSuperpixels alg = FactoryImageSegmentation.slic(new ConfigSlic(400),
		// imageType);
		ImageSuperpixels alg = FactoryImageSegmentation.fh04(new ConfigFh04(100, 30), imageType);
		// ImageSuperpixels alg = FactoryImageSegmentation.watershed(null,imageType);

		// Convert image into BoofCV format
		ImageBase colour = imageType.createImage(image.getWidth(), image.getHeight());
		ConvertBufferedImage.convertFrom(image, colour, true);

		// Segment and display results
		performSegmentation(alg, colour);

	}

	public static <T extends ImageBase<? super T>> void performSegmentation(ImageSuperpixels<? super T> alg, T colour) {
		// Segmentation often works better after blurring the image. Reduces high
		// frequency image components which
		// can cause over segmentation
		GBlurImageOps.gaussian(colour, colour, 0.5, -1, null);

		// Storage for segmented image. Each pixel will be assigned a label from 0 to
		// N-1, where N is the number
		// of segments in the image
		GrayS32 pixelToSegment = new GrayS32(colour.width, colour.height);

		// Segmentation magic happens here
		alg.segment(colour, pixelToSegment);

		// Displays the results
		visualize(pixelToSegment, colour, alg.getTotalSuperpixels());
	}

	/**
	 * Visualizes results three ways. 1) Colorized segmented image where each region
	 * is given a random color. 2) Each pixel is assigned the mean color through out
	 * the region. 3) Black pixels represent the border between regions.
	 */
	public static <T extends ImageBase<? super T>> void visualize(GrayS32 pixelToRegion, T colour, int numSegments) {
		// Computes the mean color inside each region
		ImageType<? super T> type = colour.getImageType();
		ComputeRegionMeanColor<? super T> colorize = FactorySegmentationAlg.regionMeanColor(type);

		FastQueue<float[]> segmentColor = new ColorQueue_F32(type.getNumBands());
		segmentColor.resize(numSegments);

		GrowQueue_I32 regionMemberCount = new GrowQueue_I32();
		regionMemberCount.resize(numSegments);

		ImageSegmentationOps.countRegionPixels(pixelToRegion, numSegments, regionMemberCount.data);
		colorize.process(colour, pixelToRegion, regionMemberCount, segmentColor);

		// Draw each region using their average color
		BufferedImage outColour = VisualizeRegions.regionsColor(pixelToRegion, segmentColor, null);
		// Draw each region by assigning it a random color
		BufferedImage outSegments = VisualizeRegions.regions(pixelToRegion, numSegments, null);

		// Make region edges appear red
		BufferedImage outBorder = new BufferedImage(colour.width, colour.height, BufferedImage.TYPE_INT_RGB);
		ConvertBufferedImage.convertTo(colour, outBorder, true);
		VisualizeRegions.regionBorders(pixelToRegion, 0xFF0000, outBorder);

		IP_Util_SaveBI(outColour, "ImageSegmentation_outColor");
		IP_Util_SaveBI(outBorder, "ImageSegmentation_outBorder");
		IP_Util_SaveBI(outSegments, "ImageSegmentation_outSegments");

		// anamoly

		// Show the visualization results
		ListDisplayPanel gui = new ListDisplayPanel();
		gui.addImage(outColour, "Color of Segments");
		gui.addImage(outBorder, "Region Borders");
		gui.addImage(outSegments, "Regions");
		ShowImages.showWindow(gui, "Superpixels", false);
	}

	public static void IP_Algo_Segmentation(BufferedImage inputImage) {
		// you probably don't want to segment along the image's alpha channel and the
		// code below assumes 3 channels
		inputImage = ConvertBufferedImage.stripAlphaChannel(inputImage);

		// Select input image type. Some algorithms behave different depending on image
		// type
		ImageType<Planar<GrayF32>> imageType = ImageType.pl(3, GrayF32.class);
		// ImageType<Planar<GrayU8>> imageType = ImageType.pl(3,GrayU8.class);
		// ImageType<GrayF32> imageType = ImageType.single(GrayF32.class);
		// ImageType<GrayU8> imageType = ImageType.single(GrayU8.class);

		// ImageSuperpixels alg = FactoryImageSegmentation.meanShift(null, imageType);
		// ImageSuperpixels alg = FactoryImageSegmentation.slic(new ConfigSlic(400),
		// imageType);
		ImageSuperpixels alg = FactoryImageSegmentation.fh04(new ConfigFh04(100, 30), imageType);
		// ImageSuperpixels alg = FactoryImageSegmentation.watershed(null,imageType);

		// Convert image into BoofCV format
		ImageBase colour = imageType.createImage(inputImage.getWidth(), inputImage.getHeight());
		ConvertBufferedImage.convertFrom(inputImage, colour, true);

		// Segment and display results
		performSegmentation(alg, colour);

	}

	public static void IP_SegmentColour(String imagePath) {
		BufferedImage image = UtilImageIO.loadImage(UtilIO.pathExample(imagePath));

		// Let the user select a colour
		printClickedColour(image);
		// Display pre-selected colours
		showSelectedColour("Yellow", image, 1f, 1f);
		showSelectedColour("Green", image, 1.5f, 0.65f);
		showSelectedColour("other", image, 0.5f, 0.65f);
	}

	/**
	 * Shows a colour image and allows the user to select a pixel, convert it to
	 * HSV, print the HSV values, and calls the function below to display similar
	 * pixels.
	 */

	public static void printClickedColour(final BufferedImage image) {
		ImagePanel gui = new ImagePanel(image);
		gui.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				float[] colour = new float[3];
				int rgb = image.getRGB(e.getX(), e.getY());
				ColorHsv.rgbToHsv((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF, colour);
				System.out.println("H = " + colour[0] + " S = " + colour[1] + " V = " + colour[2]);

				showSelectedColour("Selected", image, colour[0], colour[1]);

			}
		});
		ShowImages.showWindow(gui, "Colour Selector");
	}

	/**
	 * Selectively displays only pixels which have a similar hue and saturation
	 * values to what is provided. This is intended to be a simple example of colour
	 * based segmentation. Colour based segmentation can be done in RGB colour, but
	 * more problematic due to it not being intensity invariant. More robust
	 * techniques can use Gaussian models instead of a uniform distribution, as is
	 * done below.
	 * 
	 * @param args
	 */

	public static void showSelectedColour(String name, BufferedImage image, float hue, float saturation) {
		Planar<GrayF32> input = ConvertBufferedImage.convertFromPlanar(image, null, true, GrayF32.class);
		Planar<GrayF32> hsv = input.createSameShape();

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are members of
		// the selected set
		float maxDist2 = 0.4f * 0.4f;

		// Extract hue and saturation bands which are independent of intensity
		GrayF32 H = hsv.getBand(0);
		GrayF32 S = hsv.getBand(1);

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// Step through each pixel and mark how close it is to the selected colour
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++) {
			// Hue is an angle in radians, so simple subtraction doesn't work
			for (int x = 0; x < hsv.width; x++) {

				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;

				// This distance measure is a bit naive, but good enough to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;
				if (dist2 <= maxDist2) {
					output.setRGB(x, y, image.getRGB(x, y));
				}
			}
		}

		ShowImages.showWindow(output, "Showing " + name);

	}
	
	/**
	 * LABEL __ENUMs!
	 * @author Tom
	 *
	 */
	public enum DerivLevels {
		SOBELGRAY,
		SOBELX, 
		SOBELY,
		SOBELXnY,
		SOBELXX,
		SOBELXY,
		SOBELYY,
		SOBELXYX
	}
	
	public enum Tailor_Type {
		COLOURSEGMENT,
		LINEDETECTION,
		DERIVATIVE,
		ELLIPSES,
		SURF,
		SUPERPIXEL
	}
	
}

