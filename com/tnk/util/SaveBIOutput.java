package com.tnk.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import boofcv.gui.image.ShowImages;
import boofcv.io.image.UtilImageIO;

/**
 * Send a BufferedImage and a name for the file.
 * This guy will output the image to a .png in the output folder
 * 
 * @author Tom
 *
 */
public class SaveBIOutput implements Runnable{

	private String TAG = "SaveBIOutput";
	private String outputName;
	private BufferedImage bimg;
	private List<BufferedImage> biList = new ArrayList<>();
	private BufferedImage[] biArray; // = new BufferedImage[10];
	
	public SaveBIOutput(BufferedImage bi, String selectedPath) {
		outputName = selectedPath;
		bimg = bi;
		System.out.println("SaveBI :: bi loaded!");
	}
	
	public SaveBIOutput(BufferedImage[] bis, String selectedPath)
	{
		
		this.outputName = selectedPath;
		// __ISSUE FIXED #007 [BUG] This will have to cycle through ALL in the array for future enhancement
		this.biArray = bis;
		System.out.println("SaveBI :: bis loaded!");
	}
	
	public SaveBIOutput(List<BufferedImage> bis, String selectedPath)
	{
		this.outputName = selectedPath;
		this.biList.addAll(bis);
		System.out.println("SaveBI :: bis loaded!");
	}
	
	@Override
	public void run() {
		Date outputDate = new Date();
		String outputPath = new String("C:\\test\\output\\"+
				outputDate.getMonth()+"-"+
				outputDate.getDate()+" " +
				outputDate.getHours()+"h"+
				outputDate.getMinutes()+"m "+
				outputName + ".png");
		// File f = new File(outputPath);
			if (bimg!=null) {
				System.out.println("SaveBI :: Single file is loaded..."+outputPath);
				ToDisk(bimg,outputPath);
			}
			
			if (biArray!=null)
			{
				System.out.println("SaveBI :: biArray is loaded..."+outputPath);
				for (int i=0; i<biArray.length; i++)
				{
					String arrayPath = new String("C:\\test\\output\\"+
							outputDate.getMonth()+"-"+
							outputDate.getDate()+" " +
							outputDate.getHours()+"h"+
							outputDate.getMinutes()+"m "+
							outputName + i + ".png");
					ToDisk(biArray[i], arrayPath);
				}
			}
			
			// __ISSUE FIXED #011 [BUG] This code does not cycle through the array or List properly
			if (biList!=null)
			{
				System.out.println("SaveBI :: biList is loaded..."+outputPath);
				//for (int j=0; j < biList.size(); j++)
				for( ListIterator<BufferedImage> iter = biList.listIterator(); iter.hasNext();  )
				{
					BufferedImage element = iter.next();
					ShowImages.showWindow(element, "Element "+(iter.nextIndex() -1));
					System.out.println("Image " + (iter.nextIndex() -1));
					String listPath = new String("C:\\test\\output\\"+
							outputDate.getMonth()+"-"+
							outputDate.getDate()+" " +
							outputDate.getHours()+"h"+
							outputDate.getMinutes()+"m "+
							outputName + (iter.nextIndex() -1) + ".png");
					ToDisk(element, listPath);
				}
			}
			
	}	
	
	public void ToDisk(BufferedImage outImage, String outName) {
		UtilImageIO.saveImage(outImage, outName);
		System.out.println(TAG + " Saved " + outName);	
	}
}
