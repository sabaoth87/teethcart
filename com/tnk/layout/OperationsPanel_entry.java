package com.tnk.layout;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Dimension;

public class OperationsPanel_entry extends JPanel implements ChangeListener {
	private JTextField textField;
	private JSlider slider;
	private Font font = new Font("Serif", Font.PLAIN, 8);
	/**
	 * Create the entry 
	 */
		
	public OperationsPanel_entry( String operationName, String attributeName, float attributeValue,
																		 float attributeMax,
																		 float attribuateMin) {
		//this.setPreferredSize(new Dimension(260, 30));
		//this.setMaximumSize(new Dimension(260, 30));
		//this.setMinimumSize(new Dimension(260, 30));
		//this.setSize(new Dimension(260, 28));
		//this.setBounds(0, 0, 260, 25);
		
		//this.setAlignmentX(CENTER_ALIGNMENT);
		//this.setAlignmentY(CENTER_ALIGNMENT);
		this.setToolTipText(attributeName);		
		
		
		FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 0, 1);
		setLayout(flowLayout);
		
		JLabel lbl_entryName = new JLabel(operationName);
		//lbl_entryName.setSize(new Dimension(55, 14));
		//lbl_entryName.setVerticalAlignment(SwingConstants.TOP);
		lbl_entryName.setFont(font);
		add(lbl_entryName);
		
		JLabel lblNewLabel = new JLabel(attributeName);
		//lblNewLabel.setMaximumSize(new Dimension(100, 14));
		//lblNewLabel.setSize(new Dimension(55, 14));
		lblNewLabel.setFont(font);
		add(lblNewLabel);
		
		slider = new JSlider();
		slider.setSize(new Dimension(75, 25));
		slider.setPreferredSize(new Dimension(75, 25));
		slider.addChangeListener(this);
		slider.setMaximum((int) attributeMax);
		slider.setMinimum((int) attribuateMin);
		slider.setValue((int) attributeValue);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		
		
		slider.setFont(font);
		
		add(slider);
		
		textField = new JTextField();
		textField.setSize(new Dimension(25, 8));
		textField.setMaximumSize(new Dimension(40, 8));
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setText("##");
		add(textField);
		
		this.validate();
		this.repaint();
		
	}
	
	public void stateChanged(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (source.getValueIsAdjusting()) {
	    	System.out.println("Slider moving!");
	        /*int fps = (int)source.getValue();
	        if (fps == 0) {
	            if (!frozen) stopAnimation();
	        } else {
	            delay = 1000 / fps;
	            timer.setDelay(delay);
	            timer.setInitialDelay(delay * 10);
	            if (frozen) startAnimation();
	        }*/
	    	String newValue = Integer.toString(source.getValue());
	    	
	        textField.setText(newValue);
	    }
	    
	}

}
