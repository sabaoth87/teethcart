package com.tnk.layout;

/*
 *
 * Main Screen for the TeethCart Alpha Application
 *  
 *  This is the debugging and testing interface for my new Private Repository! 
 * 
 */

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageConsumer;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;

import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import javax.swing.filechooser.FileSystemView;
import javax.swing.table.TableColumn;

import com.tnk.Item_OperationInput;
import com.tnk.util.FileTableModel;
import com.tnk.util.SaveBIOutput;
import com.tnk.util.Utilities;
import com.tnk.util.Utilities.DerivLevels;
import com.tnk.util.Utilities.Tailor_Type;
import com.tnk.util.impro.ColourSegmentRunnable;
import com.tnk.util.impro.DetectFeaturesRunnable;
import com.tnk.util.impro.LineDetectRunnable;
import com.tnk.util.impro.SuperpixelRunnable;
import com.tnk.util.items.Tailor_Derivative;
import com.tnk.util.items.Tailor_Ellipses;
import com.tnk.util.items.Tailor_LineDetect;
import com.tnk.util.items.Tailor_SURF;
import com.tnk.util.items.Tailor_Superpixel;
import com.tnk.util.items.Tailor__Entry;
import com.tnk.util.items.Tailor__Queue;
import com.tnk.util.items.ImageTailor;
import com.tnk.util.items.Tailor_ColourSegment;

import boofcv.gui.ListDisplayPanel;
import boofcv.gui.image.ShowImages;
import boofcv.gui.image.VisualizeImageData;
import boofcv.io.UtilIO;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayS16;
import boofcv.struct.image.GrayU8;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class Layout_FirstScreen {

	volatile public static int progressValue;
	
	private DefaultMutableTreeNode root;
	private JTree tree;
	private JFrame frame;
	private JScrollPane previewWindow;

	/** Main GUI container */
	private JPanel gui;
	private OperationsPanel operationsList;

	/* Interface */
	private JLabel label_se; // SE scroll label; for developer callouts
	private JScrollPane scrollpane_se;

	/** Table model for File[]. */
	private FileTableModel fileTableModel;
	private ListSelectionListener listSelectionListener;
	private boolean cellSizesSet = false;
	private int rowIconPadding = 6;
	/** Used to open/edit/print files. */
	private Desktop desktop;

	/** Provides nice icons and names for files. */
	private FileSystemView fileSystemView;
	private JTable viewerTable;

	
	/**
	 * LABEL Tailors
	 */
	public Tailor_ColourSegment tailor_coloursegment;
	public Tailor_Derivative tailor_derivative;
	public Tailor_Ellipses tailor_ellipses;
	public Tailor_LineDetect tailor_linedetect;
	public Tailor_Superpixel tailor_superpixel; 
	public Tailor_SURF tailor_surf;
	public List<ImageTailor> tailorList = new ArrayList<>();
	/*
	 * File controls. private JButton openFile; private JButton printFile; private
	 * JButton editFile;
	 */

	// Directory listing
	private JTable table;
	public static JProgressBar progressBar;
	public int getProgressValue() {
		return progressBar.getValue();
	}
	
	public void setProgressValue(int value) {
		progressBar.setValue(value);
	}
	
	public void incrementProgressValue() {
		if (progressBar.getValue() < progressBar.getMaximum()) 
		{
		progressBar.setValue(progressBar.getValue()+1);
		}
	}
	/* File details. */
	private JRadioButton isFile;

	// private JTree tree;

	/*
	 * LABEL IMPRO VARS
	 * [I][P] Image Processing Controls
	 * 
	 * Thanks BoofCV!
	 * 
	 */
	public JRadioButton radio_LineDetect;
	public JRadioButton radio_FitEllipses;
	public JRadioButton radio_DetectFeatures;
	public JRadioButton radio_EasySurf;
	public JRadioButton radio_ImageDerivative;
	public JRadioButton radio_ImageFeatures;
	private JRadioButton radio_AIP; // yet to be implemented
	// __ISSUE #015 [PROP] Implement SceneRecognition for fun!
	private JRadioButton radio_SceneRecognition; // yet to implement
	private JRadioButton radio_ColorSegementation;
	public JRadioButton radio_CannyEdge;
	public JRadioButton radio_SuperPixel;
	public JRadioButton radio_AlgoAlpha;

	private BufferedImage input;
	// _TEST I am not sure about instantiating BI arrays like this...
	public static List<BufferedImage> imagesInProcess = new ArrayList<>();
	
	// ellipses
	private ListDisplayPanel ldp;
	private String selectedPath = "";

	public Item_OperationInput runBlob;
	public Item_OperationInput runImageDeriv;
	public Item_OperationInput runImageFeatures;
	public Item_OperationInput runSURF;
	public Item_OperationInput runLine;
	public Item_OperationInput runFitElip;
	public Item_OperationInput runSuperpixel;
	private Item_OperationInput runColourSegment;
	
	public Item_OperationInput runAlgoAlpha;

	public Tailor_Ellipses tailor_el;
	public Tailor_ColourSegment tailor_cs;
	public Tailor_Superpixel tailor_sp;
	Tailor__Queue opStack;
	
	
	// in the process of implementing
	// public JRadioButton radio_

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Layout_FirstScreen window = new Layout_FirstScreen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

	}

	/**
	 * Create the application.
	 */
	public Layout_FirstScreen() {
		initialize();
		runLate();
	}

	/**
	 * Run after the interface is loaded
	 */
	public void runLate() {
		// String runimg = new String();
		Utilities.DW_AddColouredText("runImageDeriv -" + String.valueOf(runImageDeriv.getEnabled()), Color.BLACK);
		Utilities.DW_AddColouredText("runImageFeatures -" + String.valueOf(runImageFeatures.getEnabled()), Color.BLACK);
		Utilities.DW_AddColouredText("runFitElip -" + String.valueOf(runFitElip.getEnabled()), Color.BLACK);
		Utilities.DW_AddColouredText("runLine -" + String.valueOf(runLine.getEnabled()), Color.BLACK);
		Utilities.DW_AddColouredText("runSURF -" + String.valueOf(runSURF.getEnabled()), Color.BLACK);
	}

	/**
	 * Initialize the contents of the frame.
	 * 	   _____________  _____________   ___________
	 *   ||  			||  			|| 			|
	 *   ||  			||  			|| 			|
	 *   ||  			||  			|| 			| 
	 *   ||  	tree	||  	table	|| 	debug	|  
	 *   ||  			||  			|| 			| 
	 *   ||  			||  			|| 			| 
	 *   || ____________||______________||__________|
	 *   ||  			||  			|| 			|
	 *   ||  			||  			|| 			|
	 *   ||  	details	||  operations 	|| 	image	|  
	 *   ||  			||  			|| 	preview	| 
	 *   || ____________||______________||__________|
	 * 
	 *  This MiG layout shit is something I am going to have to work at
	 *  
	 * 
	 */
	private void initialize() {
		
		fileSystemView = FileSystemView.getFileSystemView();
		desktop = Desktop.getDesktop();

		frame = new JFrame();
		frame.setBounds(0, 0, 1366, 768); // for my small ass monitor :'(
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout(" ", "[fill][grow][grow]",
														   "[grow][fill][fill]"));
		
		
		// create a new stack of tailors
		opStack = new Tailor__Queue();
		
		//this is for the file details
		initializeMyViewer();
		initializeMyExplorer();


		scrollpane_se = new JScrollPane();
		scrollpane_se.setViewportView(ldp);
		scrollpane_se.setBorder(new BevelBorder(BevelBorder.RAISED, Color.ORANGE, null, null, null));

		isFile = new JRadioButton();
		
		// LABEL Folder View
		// Add the File Explorer tree to the Content Pane
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateRowSorter(true);
		table.setShowVerticalLines(false);

		listSelectionListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent lse) {
				int row = table.getSelectionModel().getLeadSelectionIndex();
				// _TEST table item selection
				//setFileDetails(((FileTableModel) table.getSelectionModel().);
			}
		};

		table.getSelectionModel().addListSelectionListener(listSelectionListener);
		
		
		JScrollPane explorerPane = new JScrollPane();
		//tree.setVisibleRowCount(10);
		explorerPane.setViewportView(tree);
		
		explorerPane.setBorder(new BevelBorder(BevelBorder.RAISED, Color.darkGray, null, null, null));
		explorerPane.setAutoscrolls(true);
		
		

		JScrollPane sp_right = new JScrollPane();
		sp_right.setToolTipText("Operations");
		sp_right.setBorder(new BevelBorder(BevelBorder.RAISED, Color.darkGray, null, null, null));

		//sp_right.setMaximumSize(new Dimension(600, 600));
		//sp_right.setAlignmentY(0.0f);
		//sp_right.setAlignmentX(0.0f);

		JViewport vp_lbl_ops = new JViewport();
		JLabel lblNewLabel = new JLabel("Operation Window");
		lblNewLabel.setBorder(new BevelBorder(BevelBorder.RAISED, Color.darkGray, null, null, null));

		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBackground(Color.BLACK);
		vp_lbl_ops.add(OperationPanel());

		sp_right.setViewport(vp_lbl_ops);

		

		JScrollPane scrollpane_center = new JScrollPane();
		scrollpane_center.setViewportView(viewerTable);
		scrollpane_center.setBorder(new BevelBorder(BevelBorder.RAISED, Color.darkGray, null, null, null));
		

		// This used to be the
		// frame.getContentPane().add(scrollpane_se, "cell 1 1,grow");
		// vp_panel_right.add(scrollpane_right);

		// __ISSUE #003 [ENHANCE] Track operations with the progress bar 
		// Investigate this progress bar!
		progressBar = new JProgressBar();

		// Add the ViewerTablee to the GUI
		// frame.getContentPane().add(viewerTable, "cell 1 0,grow");
		JTextPane dbWindow = Utilities.DebugWindow();
		previewWindow = new JScrollPane();
		
		frame.setBackground(Color.YELLOW);
		explorerPane.setBackground(Color.RED);
		scrollpane_se.setBackground(Color.RED);
		sp_right.setBackground(Color.GREEN);
		scrollpane_center.setBackground(Color.RED);
		previewWindow.setBackground(Color.RED);
		
		operationsList = new OperationsPanel();
		
		frame.getContentPane().add(explorerPane, 		"cell 0 0	 , grow");
		frame.getContentPane().add(scrollpane_center, 	"cell 1 0	 , grow");
		frame.getContentPane().add(dbWindow, 			"cell 2 0	 , grow, h 200:300:400, w 200:300:400");
		frame.getContentPane().add(sp_right, 			"cell 0 1 1 1,width 400,alignx center,height 30,aligny top");
		frame.getContentPane().add(operationsList,      "cell 1 1    , grow");
		frame.getContentPane().add(previewWindow, 		"cell 2 1	 , grow, h 200:300:400, w 200:300:400");
		frame.getContentPane().add(progressBar, 		"cell 0 2 3 2, grow");

		frame.pack();
		frame.setVisible(true);
		
		/*
		 * LABEL ProgressBar Init
		 */
		progressBar.setMinimum(0);
		progressBar.setMaximum(0);
		progressBar.setValue(0);
		progressBar.setString("Welcome");
		progressBar.setStringPainted(true);
	}
	
	private void Proc_SelectedImage(String imagePath) {
		if (imagePath == null) {
			label_se.setText(imagePath);
			imagePath = "C:\\Users\\Tom\\Pictures\\download.jpg";
		}
		System.out.println(selectedPath + " is a Picture!");
		input = UtilImageIO.loadImage(UtilIO.pathExample(selectedPath));
		
		int currentX = previewWindow.getWidth();
		int currentY = previewWindow.getHeight();
		JLabel displayPreview = new JLabel(new ImageIcon(input.getScaledInstance(currentX, currentY, 0)));
		
		//previewWindow = new JScrollPane(displayPreview);
		//frame.getContentPane().remove();
		previewWindow.setViewportView(new JScrollPane(displayPreview));
		//frame.getContentPane().add(previewWindow, "cell 2 1, grow, width 100:150:200, height 100:150:200");
		
	}

	public class CreateChildNodes implements Runnable {

		private DefaultMutableTreeNode root;

		private File fileRoot;

		public CreateChildNodes(File fileRoot, DefaultMutableTreeNode root) {
			this.fileRoot = fileRoot;
			this.root = root;
		}

		@Override
		public void run() {
			createChildren(fileRoot, root);
		}

		private void createChildren(File fileRoot, DefaultMutableTreeNode node) {
			File[] files = fileRoot.listFiles();
			if (files == null)
				return;

			for (File file : files) {
				DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(new FileNode(file));
				node.add(childNode);
				if (file.isDirectory()) {
					createChildren(file, childNode);
				}
			}
		}

	}

	public class FileNode {

		private File file;

		public FileNode(File file) {
			this.file = file;
		}

		@Override
		public String toString() {
			String name = file.getName();
			if (name.equals("")) {
				return file.getAbsolutePath();
			} else {
				return name;
			}
		}
	}

	public void initializeMyExplorer() {
		/**
		 * Initialize My Explorer
		 * 
		 * | rev 00 | 2018-04-29
		 *
		 * Short method to help populate the information of the JTree File Explorer
		 * 
		 * Based on a SO answer for a simple drive explorer
		 * 
		 * - Icons? - Just show folders - Last location browsed? -> likely will have to
		 * dabble with a kind of 'User Preferences' for this
		 * 
		 */
		TreeSelectionListener treeSelectionListener = new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent tse) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) tse.getPath().getLastPathComponent();
				showChildren(node);
				setFileDetails((File) node.getUserObject());
			}
		};

		fileSystemView = FileSystemView.getFileSystemView();
		
		tree = Utilities.DriveExplorer(fileSystemView);
		tree.addTreeSelectionListener(treeSelectionListener);
		// _TEST This overrode the size of the Tree, thereby overridding the parent's ability to properly
		//       alter the size of the container, JScrollPane
		// MEMBERRY
		// _CLEAN
		//Dimension preferredSize = tree.getPreferredSize();
        //Dimension widePreferred = new Dimension(
        //    200,
        //    (int)preferredSize.getHeight());
        //tree.setPreferredSize( widePreferred );
	}

	private void initializeMyViewer() {

		viewerTable = new JTable();
		viewerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		viewerTable.setAutoCreateRowSorter(true);
		viewerTable.setShowVerticalLines(false);
		listSelectionListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent lse) {
				System.out.println("Selection changed to " + selectedPath);
				int row = viewerTable.getSelectionModel().getLeadSelectionIndex();
				if (selectedPath.endsWith(".jpg") || selectedPath.endsWith(".png")) {
					
					Proc_SelectedImage(selectedPath);
				}
				
				if (((FileTableModel) viewerTable.getModel()).getFile(row) != null) {
					
					DefaultMutableTreeNode node = new DefaultMutableTreeNode(lse);
					showChildren(node);
				setFileDetails(((FileTableModel) viewerTable.getModel()).getFile(row));
				}
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(lse);
				showChildren(node);
			setFileDetails(((FileTableModel) viewerTable.getModel()).getFile(row));
			}
		};
		viewerTable.getSelectionModel().addListSelectionListener(listSelectionListener);
	}

	public void showChildren(final DefaultMutableTreeNode node) {
		tree.setEnabled(false);
		
		//update the progress bar
		progressBar.setVisible(true);
		progressBar.setIndeterminate(true);

		SwingWorker<Void, File> worker = new SwingWorker<Void, File>() {
			@Override
			public Void doInBackground() {
				File file = (File) node.getUserObject();
				if (file.isDirectory()) {
					File[] files = fileSystemView.getFiles(file, true); // This is apparently a very important step or a
																		// commonly missed caveat!
					if (node.isLeaf()) {
						for (File child : files) {
							if (child.isDirectory()) {
								publish(child);
							}
						}
					}
					setTableData(files);
				}
				return null;
			}

			@Override
			protected void process(List<File> chunks) {
				for (File child : chunks) {
					node.add(new DefaultMutableTreeNode(child));
				}
			}

			@Override
			protected void done() {
				progressBar.setIndeterminate(false);
				progressBar.setVisible(false);
				tree.setEnabled(true);
			}
		};
		worker.execute();
	}

	/** Update the table on the EDT */
	private void setTableData(final File[] files) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (fileTableModel == null) {
					fileTableModel = new FileTableModel();
					viewerTable.setModel(fileTableModel);
				}
				viewerTable.getSelectionModel().removeListSelectionListener(listSelectionListener);
				fileTableModel.setFiles(files);
				viewerTable.getSelectionModel().addListSelectionListener(listSelectionListener);
				if (!cellSizesSet) {
					Icon icon = fileSystemView.getSystemIcon(files[0]);

					// size adjustment to better account for icons
					viewerTable.setRowHeight(icon.getIconHeight() + rowIconPadding);

					setColumnWidth(0, -1);
					setColumnWidth(3, 60);
					viewerTable.getColumnModel().getColumn(3).setMaxWidth(120);
					//setColumnWidth(4, -1);
					//setColumnWidth(5, -1);
					//setColumnWidth(6, -1);
					//setColumnWidth(7, -1);
					//setColumnWidth(8, -1);
					//setColumnWidth(9, -1);

					cellSizesSet = true;
				}
			}
		});
	}

	private void setColumnWidth(int column, int width) {
		TableColumn tableColumn = viewerTable.getColumnModel().getColumn(column);
		if (width < 0) {
			// use the preferred width of the header..
			JLabel label = new JLabel((String) tableColumn.getHeaderValue());
			Dimension preferred = label.getPreferredSize();
			// altered 10->14 as per camickr comment.
			width = (int) preferred.getWidth() + 14;
		}
		tableColumn.setPreferredWidth(width);
		tableColumn.setMaxWidth(width);
		tableColumn.setMinWidth(width);
	}

	/** Update the File details view with the details of this File. */
	private void setFileDetails(File file) {
		// currentFile = file;
		
		//_TRY find a use for these icons 
		//Icon icon = fileSystemView.getSystemIcon(file);

		selectedPath = file.getPath();

		/*
		 * if (selectedPath.endsWith(".jpg") ) { Proc_SelectedImage(selectedPath); }
		 */
		
		if (file != null) {
		isFile.setSelected(file.isFile());
		}
		if (gui != null) {
		
		JFrame f = (JFrame) gui.getTopLevelAncestor();
		if (f != null) {
			f.setTitle(

					"Browsing :: " + fileSystemView.getSystemDisplayName(file));
		}
		
		System.out.println("Selected " + selectedPath);
		Utilities.DW_AddColouredText(file.getAbsolutePath(), Color.ORANGE);
		if (selectedPath.endsWith(".png") || selectedPath.endsWith(".jpg") ||
				selectedPath.endsWith(".bmp") || selectedPath.endsWith(".gif"))
		{
			input = UtilImageIO.loadImage(UtilIO.pathExample(file.getPath()));
			
			JLabel displayPreview = new JLabel(new ImageIcon(input));
			
			//previewWindow.setViewportView(displayPreview);
		}
		else {
			label_se.setText(file.getPath());
		}
		gui.repaint();
		}
	}

	public JPanel OperationPanel() {

		JPanel jp = new JPanel(new MigLayout("wrap 3, top, center", 
				"20[fill]20",
				   "[fill]"));
		jp.add(new JLabel("--", JLabel.CENTER));
		jp.add(new JLabel("OPS", JLabel.CENTER));
		jp.add(new JLabel("--", JLabel.CENTER));
		
		runImageDeriv = new Item_OperationInput();
		runImageDeriv.setEnabled(false);
		radio_ImageDerivative = new JRadioButton("Derivate Image");
		runImageFeatures = new Item_OperationInput();
		runImageFeatures.setEnabled(false);
		radio_ImageFeatures = new JRadioButton("Image Features");
		runSURF = new Item_OperationInput();
		runSURF.setEnabled(false);
		radio_EasySurf = new JRadioButton("SURF");
		runLine = new Item_OperationInput();
		runLine.setEnabled(false);
		radio_LineDetect = new JRadioButton("Line Detection");
		runFitElip = new Item_OperationInput();
		runFitElip.setEnabled(false);
		radio_FitEllipses = new JRadioButton("Fit Ellipses");
		runSuperpixel = new Item_OperationInput();
		runSuperpixel.setEnabled(false);
		radio_SuperPixel = new JRadioButton("Superpixel Image");
		
		runColourSegment = new Item_OperationInput();
		runColourSegment.setEnabled(false);
		radio_ColorSegementation = new JRadioButton("Segment Colour");
		
		
		runAlgoAlpha = new Item_OperationInput();
		runAlgoAlpha.setEnabled(false);
		radio_AlgoAlpha = new JRadioButton("Alpha");
		

		// Contract_OperationInput runBlob = new Contract_OperationInput();
		// runBlobRadio = new JRadioButton("Blob Detection");
		// Contract_OperationInput runAIP = new Contract_OperationInput();
		// runAIPRadio = new JRadioButton("Associate Interest Points");

		
		JButton runIPButton = new JButton();/*
		runIPButton.setSize(new Dimension(25, 8));
		runIPButton.setPreferredSize(new Dimension(25, 7));
		runIPButton.setMaximumSize(new Dimension(25, 8));
		runIPButton.setMinimumSize(new Dimension(25, 8));*/
		runIPButton.setEnabled(false);
		runIPButton.setText("SELECT A PROCESS");
		// LABEL runIPButton click
		
		radio_ImageDerivative.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				
				if (runImageDeriv.getEnabled()) {
					runImageDeriv.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runImageDeriv.setEnabled(true);
					/**
					 * __ISSUE #009 [PROP] Trying to load operation parameters into the GUI
					 * __ISSUE #010 [ENHANCE] Size issues - limit the slider maybe?
					 * _ENHANCE If I could combine the Tailors and the OperationsEntry into one object
					 * or develop some kind of real-time relation. Thereby enabling me to have the modular,
					 * but also being able to have the GUI interact with the attributes freely, and
					 * independently (individually?).
					 */
					
					// operationsList
					if (operationsList.addOperation("Derivative", "Level", 0, 4, 0)) {
						System.out.println("Derivative added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 1f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.DERIVATIVE, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
		
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("Priming Derivator     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		radio_ImageFeatures.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Features Radio TOUCHED", Color.BLACK);
				if (runImageFeatures.getEnabled()) {
					runImageFeatures.setEnabled(false);
					
					runIPButton.setEnabled(true);
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runImageFeatures.setEnabled(true);
					
					// operationsList
					if (operationsList.addOperation("Features", " Max", 0, 10, 0)) {
						System.out.println("Feature Finder added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 10f;
						//add the operation to the stack
						// _FIX Feature Finder Tailor?
						opStack.addToStack(new Tailor__Entry(Tailor_Type.SURF, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}

					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("Loading Feature Finder...     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		radio_EasySurf.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("SURF Radio TOUCHED", Color.BLACK);
				if (runSURF.getEnabled()) {
					runSURF.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runSURF.setEnabled(true);
					// operationsList
					if (operationsList.addOperation("SURF", " Max", 0, 10, 0)) {
						System.out.println("SURF added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 10f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.SURF, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("SURFs up... Queued up, that is... (ahem)     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		radio_LineDetect.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Line Radio TOUCHED", Color.BLACK);
				if (runLine.getEnabled()) {
					runLine.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runLine.setEnabled(true);
					// operationsList
					if (operationsList.addOperation("Lines", " Max", 0, 7f, 0)) {
						System.out.println("Line Finder added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 7f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.LINEDETECTION, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("Lines queued!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		radio_FitEllipses.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Ellipses Radio TOUCHED", Color.BLACK);
				if (runFitElip.getEnabled()) {
					runFitElip.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runFitElip.setEnabled(true);
					// operationsList
					if (operationsList.addOperation("Ellipses", " Max", 0, 10, 0)) {
						System.out.println("Ellipses added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 10f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.ELLIPSES, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("Lets Fit Some Ellips     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});

		radio_SuperPixel.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Superpixel Radio TOUCHED", Color.BLACK);
				if (runSuperpixel.getEnabled()) {
					runSuperpixel.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runSuperpixel.setEnabled(true);
					// operationsList
					if (operationsList.addOperation("Superpixels", " ", 0, 1, 0)) {
						System.out.println("Superpixelation added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 1f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.SUPERPIXEL, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("SUPERPIXEL, TO WORK!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		
		radio_ColorSegementation.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Colour Segment Radio TOUCHED", Color.BLACK);
				if (runColourSegment.getEnabled()) {
					runColourSegment.setEnabled(false);
					
					progressBar.setMaximum(progressBar.getMaximum()-1);
				} else {
					// _TEST testing using the opStack instead of boolean checks
					//runColourSegment.setEnabled(true);
					// operationsList
					if (operationsList.addOperation("ColSeg", " Hue", 0, 10, 0)) {
						System.out.println("Colour Segmentation added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 10f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.COLOURSEGMENT, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					
					progressBar.setMaximum(progressBar.getMaximum()+1);
					progressBar.setString("Queueing up te Colour Picker!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});
		
		radio_AlgoAlpha.addItemListener(new ItemListener() {
			@Override

			public void itemStateChanged(ItemEvent arg0) {
				Utilities.DW_AddColouredText("Algo Alpha Radio TOUCHED", Color.BLACK);
				if (runAlgoAlpha.getEnabled()) {
					runAlgoAlpha.setEnabled(false);
				} else {
					runAlgoAlpha.setEnabled(true);
					
					
					if (operationsList.addOperation("ColSeg", " Hue", 0, 10, 0)) {
						System.out.println("Colour Segmentation added to queue");
						//load the attributes
						float[] attributes = new float[1];
						attributes[0] = 10f;
						//add the operation to the stack
						opStack.addToStack(new Tailor__Entry(Tailor_Type.SUPERPIXEL, selectedPath, attributes));
						opStack.addToStack(new Tailor__Entry(Tailor_Type.COLOURSEGMENT, selectedPath, attributes));
						opStack.addToStack(new Tailor__Entry(Tailor_Type.ELLIPSES, selectedPath, attributes));
						//execute the stack of operation(s)
						//opStack.executeStack();
						operationsList.repaint();
						frame.repaint();
					}
					
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
					// Algorithm
					progressBar.setMaximum(progressBar.getMaximum()+3);
					progressBar.setString("Queueing up the FIRST ALGORITHM     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					runIPButton.setEnabled(true);
					runIPButton.setText("RUN SELECTED");
				}
			}
		});

		jp.add(radio_ImageDerivative);
		jp.add(radio_ImageFeatures);
		jp.add(radio_EasySurf);
		jp.add(radio_LineDetect);
		jp.add(radio_FitEllipses);
		jp.add(radio_SuperPixel);
		jp.add(radio_ColorSegementation);
		
		jp.add(radio_AlgoAlpha);

		runIPButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Utilities.DW_AddColouredText("Button PRESSED", Color.BLUE);

				/**
				 * LABEL RUN::Derivative
				 */
				if (runImageDeriv.getEnabled()) {
					Tailor_Derivative opDeriv = new Tailor_Derivative(DerivLevels.SOBELXnY, selectedPath);

					// __ISSUE #017 [PROP] Create a public String to store and pass the selected image's path
					// See if we can make a public variable for the imagePath to label in the progress bar?
					
					Thread threadDeriv = new Thread(opDeriv);
					progressBar.setString("Running Derivative " + 
										   progressBar.getValue() +
										   "/" + progressBar.getMaximum() + 
										   "_" + progressBar.getPercentComplete());
					
					threadDeriv.start();
					progressBar.setValue(progressBar.getValue()+1);
					
					progressBar.setString("Complete" + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					radio_ImageDerivative.setSelected(false);
					//Utilities.IP_ImageDerivative(selectedPath);
					
				}

				if (runImageFeatures.getEnabled()) {
					Utilities.DW_AddColouredText("Image Features Enabled", Color.BLUE);
					progressBar.setString("Finding Features... " + 
										   progressBar.getValue() +
										   "/" + progressBar.getMaximum() + 
										   "_" + progressBar.getPercentComplete());
					
					
					Utilities.DW_AddColouredText("Finding Image Features in:", Color.BLUE);
					Utilities.DW_AddColouredText(selectedPath, Color.black);
					Utilities.IPa_DetectFeatures(input, GrayF32.class);
					DetectFeaturesRunnable df = new DetectFeaturesRunnable(selectedPath);
					Thread featureDetect = new Thread(df);
					featureDetect.setName(selectedPath);
					//df.
					featureDetect.start();
					progressBar.setValue(progressBar.getValue()+1);
					
					progressBar.setString("Complete" + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					radio_ImageFeatures.setSelected(false);
				}
				if (runSURF.getEnabled()) {
					Utilities.DW_AddColouredText("Easy Surf Enabled", Color.BLUE);

					progressBar.setString("SURFing...tubular!  " + 
										   progressBar.getValue() +
										   "/" + progressBar.getMaximum() + 
										   "_" + progressBar.getPercentComplete());
					
					Utilities.IP_EasySURF(selectedPath);
					
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString("Complete!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					radio_EasySurf.setSelected(false);
				}
				
				/**
				 * LABEL RUN::Lines
				 */
				if (runLine.getEnabled()) {
					
					progressBar.setString("Looking for lines... " + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					
					//Utilities.IP_LineDetect(selectedPath, GrayU8.class, GrayS16.class, 7);
					LineDetectRunnable ld = new LineDetectRunnable(selectedPath);
					Thread lineDetect = new Thread(ld);
					lineDetect.setName(selectedPath);
					
					lineDetect.run();
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString(" z3u|7 Complete" + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					radio_LineDetect.setSelected(false);
				}
				
				/**
				 * LABEL RUN::Ellipses
				 */
				
				if (runFitElip.getEnabled()) {
					Utilities.DW_AddColouredText("Fit Ellippses Enabled", Color.BLUE);

					// create the local instance of the class
					tailor_el = new Tailor_Ellipses(2, selectedPath);
					// load the class instance into the thread handler
					Thread ellipThread = new Thread(tailor_el);
					// add the selectedPath to the handlers interface
					ellipThread.setName(selectedPath);		
					
					// update the progressBar
					progressBar.setString("Drawing circles... " + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					
					// LABEL RUN :: ELLIPSE THREAD
					ellipThread.run();
					// _TEST Trying to see if I can retrieve an image from this operation class after the threat is run
					System.out.println("Attempting to retrieve the output");
					// retrieve the output from the ellipse runnable
					imagesInProcess = tailor_el.getOutput();
					// test to see if the output is null, wrap with a debugMode check
					// for the System.out portion
					if (imagesInProcess==null)
					{
						System.out.println("Image was null :(  ");
					}
					else {
						System.out.println("Image loaded :: " + tailor_el.getOutput().size() + " items add");
						System.out.println("Stack is now " + imagesInProcess.size());
					}
					
					// _TEST Ellipses SaveBIOutput
					Runnable testOut = new SaveBIOutput(imagesInProcess, "  TTEESSTT  ");
					new Thread(testOut).run();
					
					// update the UI that FitEllipse has concluded
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString(" Ellipses Complete" + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					//Utilities.IPa_FitEllipses(selectedPath);
					radio_FitEllipses.setSelected(false);
				}
				
				/**
				 * LABEL RUN::Superpixel
				 */
				if (runSuperpixel.getEnabled()) {
					progressBar.setString("Superpixelating... " + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					
					// _CLEAN
					//SuperpixelRunnable sr = new SuperpixelRunnable(selectedPath);
					//Thread superpix = new Thread(sr);
					//superpix.setName(selectedPath);
					//superpix.start();
					
					// create the local instance of the class
					tailor_sp = new Tailor_Superpixel(2, selectedPath);
					// load the class instance into the thread handler
					Thread superPixel_thread = new Thread(tailor_sp);
					// add the selectedPath to the handlers interface
					superPixel_thread.setName(selectedPath);		
					
					// LABEL RUN :: SUPERPIXEL THREAD
					superPixel_thread.run();
					// _TEST Trying to see if I can retrieve an image from this operation class after the threat is run
					System.out.println("Attempting to retrieve the SuperPixel output...");
					// retrieve the output from the ellipse runnable
					imagesInProcess = tailor_sp.getOutput();
					// test to see if the output is null, wrap with a debugMode check
					// for the System.out portion
					
					if (imagesInProcess==null)
					{
						System.out.println("Womp Womp - Image was null :(  ");
					}
					else {
						System.out.println("Image loaded :: " + tailor_sp.getOutput().size() + " items add");
						System.out.println("Stack is now :: " + imagesInProcess.size() + " high!");
					}
					
					// _TEST SuperPixel SaveBIOutput
					// _ENHANCE wrap in debug
					Runnable testOut = new SaveBIOutput(imagesInProcess, "_spTest_");
					new Thread(testOut).start();
					
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString("COMPLETE I am pixelated out..." + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					radio_SuperPixel.setSelected(false);
				}
				
				/**
				 * LABEL RUN::Colour Segment
				 */
				if (runColourSegment.getEnabled()) {
					
					// create the local instance of the class
					tailor_cs = new Tailor_ColourSegment(100, selectedPath);
					// load the class instance into the thread handler
					Thread colourseg = new Thread (tailor_cs);
					// add the selectedPath to the handlers interface
					colourseg.setName(selectedPath);
					
					progressBar.setString("Segmenting Colours... " + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					
					//ColourSegmentRunnable csr = new ColourSegmentRunnable(selectedPath);
					
					
					// LABEL RUN :: COLOUR SEGMENT THREAD
					colourseg.start();
					// _TEST Trying to see if I can retrieve an image from this operation class after the threat is run
					System.out.println("Attempting to retrieve the Colour Seg output");
					// retrieve the output from the ellipse runnable
					/*List<BufferedImage> colourSegIncoming = tailor_cs.getOutput();
					imagesInProcess.addAll(colourSegIncoming);
					// test to see if the output is null, wrap with a debugMode check
					// for the System.out portion
					if (tailor_cs.getOutput()==null)
					{
						System.out.println("Colour Segment output was null :(  ");
					}
					else {
						System.out.println("Colour Segment output loaded :: " + tailor_cs.getOutput().size() + " items add");
						System.out.println("Stack is now " + imagesInProcess.size());
					}
					*/
					// _TEST Colour Segment SaveBIOutput
					System.out.println("Loading ColourSegOutput...");
					Runnable test_ColourSegOut = new SaveBIOutput(imagesInProcess, "TEST_ColSeg");
					new Thread(test_ColourSegOut).start();
					
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString("Complete!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					radio_ColorSegementation.setSelected(false);
					}
				
				if (runAlgoAlpha.getEnabled()) {
					
					progressBar.setString("Initiating Alpha... " + 
							   progressBar.getValue() +
							   "/" + progressBar.getMaximum() + 
							   "_" + progressBar.getPercentComplete());
					
					// Utilities.IP_ImageSegmentation(selectedPath);
					FirstFinderAlgorithm(selectedPath);
					progressBar.setValue(progressBar.getValue()+1);
					progressBar.setString("Complete!     "
							+ progressBar.getValue() + "/"
							+ progressBar.getMaximum() + " Jobs complete");
					radio_AlgoAlpha.setSelected(false);
				}
				
				// _TEST Testing the progressBar's status update(s)
				int testMax = progressBar.getMaximum();
				int testVal = progressBar.getValue();
				if (testVal <= testMax) {
					progressBar.setIndeterminate(false);
					runIPButton.setEnabled(false);
					runIPButton.setText("SELECT A PROCESS");
				}
				opStack.executeStack();
			}
		});

		jp.add(runIPButton, "cell 0 4 1 4");

		return jp;
	}
	
	public void FirstFinderAlgorithm(String imagePath) {
		/*		
		Utilities.IPa_DetectFeatures(Utilities.IP_Algo_Deriv(imagePath), GrayF32.class);
		progressBar.setValue(1);
		Utilities.IP_Algo_Segmentation(Utilities.IP_Algo_Deriv(imagePath));
		progressBar.setValue(2);
		progressBar.setString("First Finder Algorithm COMPLETE");
		*/
		
		/*
		 
		 * Operation 0
		 
		System.out.println("OP1 START");
		//Tailor_Derivative step0 = new Tailor_Derivative(DerivLevels.SOBELXnY, imagePath);
		Tailor_Superpixel step0 = new Tailor_Superpixel(1, imagePath);
		Thread step0Thread = new Thread(step0);
		step0Thread.setName(selectedPath);
		step0Thread.run();
		
		
		
		 * Operation 1
		 
		System.out.println("OP2 START");
		Tailor_Ellipses step1 = new Tailor_Ellipses(4f, imagesInProcess.get(0));
		Thread step1Thread = new Thread(step1);
		step1Thread.setName(selectedPath);
		step1Thread.run();
		*/
		
		//tailorList.add(tailor_superpixel);
		//tailorList.add(tailor_ellipses);
		
		float[] att1 = new float[1];
		att1[0] =1f;
		
		opStack.addToStack(new Tailor__Entry(Tailor_Type.SUPERPIXEL, selectedPath, att1));
		opStack.addToStack(new Tailor__Entry(Tailor_Type.COLOURSEGMENT, selectedPath, att1));
		opStack.addToStack(new Tailor__Entry(Tailor_Type.ELLIPSES, selectedPath, att1));
		opStack.executeStack();
		
		//CycleThroughTailors();
		
		// _TEST AlgoAlpha SaveBIOutput
		Runnable testOut = new SaveBIOutput(imagesInProcess, "_alpha_");
		new Thread(testOut).run();
	}
	

	public static void SetInProcessImage (List<BufferedImage> incoming) {
		System.out.println("New Image Incoming!");
		if (imagesInProcess==null)
		{
			System.out.println("Incoming was null :(  ");
		}
		else {
			System.out.println("Incoming loaded :: " + incoming.size() + " items inside");
			// add all images of the incoming to the current stack in process
			imagesInProcess.addAll(incoming);
			// set the main image of the incoming to the top of the stack
			imagesInProcess.set(0, incoming.get(0));
		}
		
	}
	
	public static List<BufferedImage> GetInProcessImages (){
		return imagesInProcess;
	}
	
	
	public void CycleThroughTailors() {
		Date outputDate = new Date();
		System.out.println("LayoutFirstScreen :: ");
		for( ListIterator<ImageTailor> iter = tailorList.listIterator(); iter.hasNext();  )
		{
			ImageTailor element = iter.next();
			// ShowImages.showWindow(element, "Element "+(iter.nextIndex() -1));
			System.out.println("Tailor " + (iter.nextIndex() -1));
			
			RunATailor(element);
		}
	}
	
	public boolean RunATailor(ImageTailor tailor)
	{		
		
		// tailor = new tailor(3f, imagesInProcess.get(0));
		tailor.run();		
		return false;
	}
	
	public void TestATailor(ImageTailor tailor)
	{
		
		
	}
	
}
