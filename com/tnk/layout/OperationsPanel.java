package com.tnk.layout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import java.awt.FlowLayout;
import java.awt.GridLayout;

public class OperationsPanel extends JPanel {

	public JPanel opsEntry;
	
	/**
	 * Create the panel.
	 */
	public OperationsPanel() {
		
		// _TEST
		//setLayout(new FlowLayout(FlowLayout.CENTER));
		setLayout(new MigLayout("wrap 1, top, center", "20[]20",
				   "[fill]"));

	}
	/**
	 * __ISSUE #012 [TASK] Complete addOperation
	 * 
	 * OperationsPanel_entry may have to be updated to receive Tailors
	 * 
	 * @param opName
	 * @param attName
	 * @param attValue
	 * @param attMax
	 * @param attMin
	 * @return
	 */
	public boolean addOperation( String opName, String attName,float attValue, float attMax, float attMin) {
		
		opsEntry = new OperationsPanel_entry(opName, attName, attValue, attMax, attMin);
		
		this.add(opsEntry);
		this.validate();
		this.repaint();
		return true;
	}
	
	//public boolean addOperation( )

}
